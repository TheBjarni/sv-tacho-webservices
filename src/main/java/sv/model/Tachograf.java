package sv.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by tymek on 27.03.17.
 */
@Entity
@SequenceGenerator( name = "SEQ_TACHO", sequenceName = "tachograf_tch_id_seq")
@Table(name = "tachograf")
public class Tachograf implements Serializable, Cloneable {
    private Integer tchId;
    private String tchNumerSeryjnyEw;
    private String tchModelEw;
    private String tchSymbolEw;
    private String tchOdtVin;
    private String tchOdtNrRejestracyjny;
    private String tchOdtModel;
    private Timestamp tchDataMontazuEw;
    private Short tchOdczytyCoIleDni;
    private Short tchOdczytyIleRazyWMiesiacu;
    private Short tchOdczytyKtoryTydzien;
    private String tchEmailDoOstrzezen;
    private Timestamp tchZlecenieOdczytuTeraz;
    private Integer tchKlnId;
    private Integer tchAdnId;
    private Rejestrator tchRejestratorSerial;
    private Timestamp tchDataCzasPierwszejPozycji;
    private Timestamp tchDaneTachografuOd;
    private Timestamp tchDaneTachografuDo;
    private Boolean tchPrzerwijBiezacyOdczyt;
    private String tchTypZakresOdczytu;
    private List<OdczytTachografu> odczyty;

    private Date dataOstatniegoOdczytu;
    private Date dataOstatniejProbyOdczytu;
    private Date dataOstatniejAutoryzacji;

    public void setTchId(Integer tchId) {
        this.tchId = tchId;
    }

    @Id
    @Column(name = "tch_id")
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SEQ_TACHO")
    public Integer getTchId() {
        return tchId;
    }

    @Basic
    @Column(name = "tch_numer_seryjny_ew")
    public String getTchNumerSeryjnyEw() {
        return tchNumerSeryjnyEw;
    }

    public void setTchNumerSeryjnyEw(String tchNumerSeryjnyEw) {
        this.tchNumerSeryjnyEw = tchNumerSeryjnyEw;
    }

    @Basic
    @Column(name = "tch_model_ew")
    public String getTchModelEw() {
        return tchModelEw;
    }

    public void setTchModelEw(String tchModelEw) {
        this.tchModelEw = tchModelEw;
    }

    @Basic
    @Column(name = "tch_symbol_ew")
    public String getTchSymbolEw() {
        return tchSymbolEw;
    }

    public void setTchSymbolEw(String tchSymbolEw) {
        this.tchSymbolEw = tchSymbolEw;
    }

    @Basic
    @Column(name = "tch_odt_vin")
    public String getTchOdtVin() {
        return tchOdtVin;
    }

    public void setTchOdtVin(String tchOdtVin) {
        this.tchOdtVin = tchOdtVin;
    }

    @Basic
    @Column(name = "tch_odt_nr_rejestracyjny")
    public String getTchOdtNrRejestracyjny() {
        return tchOdtNrRejestracyjny;
    }

    public void setTchOdtNrRejestracyjny(String tchOdtNrRejestracyjny) {
        this.tchOdtNrRejestracyjny = tchOdtNrRejestracyjny;
    }

    @Basic
    @Column(name = "tch_odt_model")
    public String getTchOdtModel() {
        return tchOdtModel;
    }

    public void setTchOdtModel(String tchOdtModel) {
        this.tchOdtModel = tchOdtModel;
    }

    @Basic
    @Column(name = "tch_data_montazu_ew")
    public Timestamp getTchDataMontazuEw() {
        return tchDataMontazuEw;
    }

    public void setTchDataMontazuEw(Timestamp tchDataMontazuEw) {
        this.tchDataMontazuEw = tchDataMontazuEw;
    }

    @Basic
    @Column(name = "tch_odczyty_co_ile_dni")
    public Short getTchOdczytyCoIleDni() {
        return tchOdczytyCoIleDni;
    }

    public void setTchOdczytyCoIleDni(Short tchOdczytyCoIleDni) {
        this.tchOdczytyCoIleDni = tchOdczytyCoIleDni;
    }

    @Basic
    @Column(name = "tch_odczyty_ile_razy_w_miesiacu")
    public Short getTchOdczytyIleRazyWMiesiacu() {
        return tchOdczytyIleRazyWMiesiacu;
    }

    public void setTchOdczytyIleRazyWMiesiacu(Short tchOdczytyIleRazyWMiesiacu) {
        this.tchOdczytyIleRazyWMiesiacu = tchOdczytyIleRazyWMiesiacu;
    }

    @Basic
    @Column(name = "tch_odczyty_ktory_tydzien")
    public Short getTchOdczytyKtoryTydzien() {
        return tchOdczytyKtoryTydzien;
    }

    public void setTchOdczytyKtoryTydzien(Short tchOdczytyKtoryTydzien) {
        this.tchOdczytyKtoryTydzien = tchOdczytyKtoryTydzien;
    }

    @Basic
    @Column(name = "tch_email_do_ostrzezen")
    public String getTchEmailDoOstrzezen() {
        return tchEmailDoOstrzezen;
    }

    public void setTchEmailDoOstrzezen(String tchEmailDoOstrzezen) {
        this.tchEmailDoOstrzezen = tchEmailDoOstrzezen;
    }

    @Basic
    @Column(name = "tch_zlecenie_odczytu_teraz")
    public Timestamp getTchZlecenieOdczytuTeraz() {
        return tchZlecenieOdczytuTeraz;
    }

    public void setTchZlecenieOdczytuTeraz(Timestamp tchZlecenieOdczytuTeraz) {
        this.tchZlecenieOdczytuTeraz = tchZlecenieOdczytuTeraz;
    }

    @Basic
    @Column(name = "tch_kln_id")
    public Integer getTchKlnId() {
        return tchKlnId;
    }

    public void setTchKlnId(Integer tchKlnId) {
        this.tchKlnId = tchKlnId;
    }

    @Basic
    @Column(name = "tch_adn_id")
    public Integer getTchAdnId() {
        return tchAdnId;
    }

    public void setTchAdnId(Integer tchAdnId) {
        this.tchAdnId = tchAdnId;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tch_rejestrator_serial")
    public Rejestrator getTchRejestratorSerial() {
        return tchRejestratorSerial;
    }

    public void setTchRejestratorSerial(Rejestrator tchRejestratorSerial) {
        this.tchRejestratorSerial = tchRejestratorSerial;
    }

    @Basic
    @Column(name = "tch_data_czas_pierwszej_pozycji")
    public Timestamp getTchDataCzasPierwszejPozycji() {
        return tchDataCzasPierwszejPozycji;
    }

    public void setTchDataCzasPierwszejPozycji(Timestamp tchDataCzasPierwszejPozycji) {
        this.tchDataCzasPierwszejPozycji = tchDataCzasPierwszejPozycji;
    }

    @Basic
    @Column(name = "tch_dane_tachografu_od")
    public Timestamp getTchDaneTachografuOd() {
        return tchDaneTachografuOd;
    }

    public void setTchDaneTachografuOd(Timestamp tchDaneTachografuOd) {
        this.tchDaneTachografuOd = tchDaneTachografuOd;
    }

    @Basic
    @Column(name = "tch_dane_tachografu_do")
    public Timestamp getTchDaneTachografuDo() {
        return tchDaneTachografuDo;
    }

    public void setTchDaneTachografuDo(Timestamp tchDaneTachografuDo) {
        this.tchDaneTachografuDo = tchDaneTachografuDo;
    }

    @Basic
    @Column(name = "tch_przerwij_biezacy_odczyt")
    public Boolean getTchPrzerwijBiezacyOdczyt() {
        if (tchPrzerwijBiezacyOdczyt == null) {
            return false;
        }
        return tchPrzerwijBiezacyOdczyt;
    }

    public void setTchPrzerwijBiezacyOdczyt(Boolean tchPrzerwijBiezacyOdczyt) {
        this.tchPrzerwijBiezacyOdczyt = tchPrzerwijBiezacyOdczyt;
    }

    @Basic
    @Column(name = "tch_typ_zakres_odczytu")
    public String getTchTypZakresOdczytu() {
        return tchTypZakresOdczytu;
    }

    public void setTchTypZakresOdczytu(String tchTypZakresOdczytu) {
        this.tchTypZakresOdczytu = tchTypZakresOdczytu;
    }

    @OneToMany(mappedBy="owner")
    public List<OdczytTachografu> getOdczyty() {
        return odczyty;
    }

    public void setOdczyty(List<OdczytTachografu> odczyty) {
        this.odczyty = odczyty;
    }

    @Transient
    public Date getDataOstatniegoOdczytu() {
        return dataOstatniegoOdczytu;
    }

    public void setDataOstatniegoOdczytu(Date dataOstatniegoOdczytu) {
        this.dataOstatniegoOdczytu = dataOstatniegoOdczytu;
    }

    @Transient
    public Date getDataOstatniejProbyOdczytu() {
        return dataOstatniejProbyOdczytu;
    }

    public void setDataOstatniejProbyOdczytu(Date dataOstatniejProbyOdczytu) {
        this.dataOstatniejProbyOdczytu = dataOstatniejProbyOdczytu;
    }

    @Transient
    public Date getDataOstatniejAutoryzacji() {
        return dataOstatniejAutoryzacji;
    }

    public void setDataOstatniejAutoryzacji(Date dataOstatniejAutoryzacji) {
        this.dataOstatniejAutoryzacji = dataOstatniejAutoryzacji;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tachograf tachograf = (Tachograf) o;

        if (tchId != null ? !tchId.equals(tachograf.tchId) : tachograf.tchId != null) return false;
        if (tchNumerSeryjnyEw != null ? !tchNumerSeryjnyEw.equals(tachograf.tchNumerSeryjnyEw) : tachograf.tchNumerSeryjnyEw != null)
            return false;
        if (tchModelEw != null ? !tchModelEw.equals(tachograf.tchModelEw) : tachograf.tchModelEw != null) return false;
        if (tchSymbolEw != null ? !tchSymbolEw.equals(tachograf.tchSymbolEw) : tachograf.tchSymbolEw != null)
            return false;
        if (tchOdtVin != null ? !tchOdtVin.equals(tachograf.tchOdtVin) : tachograf.tchOdtVin != null) return false;
        if (tchOdtNrRejestracyjny != null ? !tchOdtNrRejestracyjny.equals(tachograf.tchOdtNrRejestracyjny) : tachograf.tchOdtNrRejestracyjny != null)
            return false;
        if (tchOdtModel != null ? !tchOdtModel.equals(tachograf.tchOdtModel) : tachograf.tchOdtModel != null)
            return false;
        if (tchDataMontazuEw != null ? !tchDataMontazuEw.equals(tachograf.tchDataMontazuEw) : tachograf.tchDataMontazuEw != null)
            return false;
        if (tchOdczytyCoIleDni != null ? !tchOdczytyCoIleDni.equals(tachograf.tchOdczytyCoIleDni) : tachograf.tchOdczytyCoIleDni != null)
            return false;
        if (tchOdczytyIleRazyWMiesiacu != null ? !tchOdczytyIleRazyWMiesiacu.equals(tachograf.tchOdczytyIleRazyWMiesiacu) : tachograf.tchOdczytyIleRazyWMiesiacu != null)
            return false;
        if (tchOdczytyKtoryTydzien != null ? !tchOdczytyKtoryTydzien.equals(tachograf.tchOdczytyKtoryTydzien) : tachograf.tchOdczytyKtoryTydzien != null)
            return false;
        if (tchEmailDoOstrzezen != null ? !tchEmailDoOstrzezen.equals(tachograf.tchEmailDoOstrzezen) : tachograf.tchEmailDoOstrzezen != null)
            return false;
        if (tchZlecenieOdczytuTeraz != null ? !tchZlecenieOdczytuTeraz.equals(tachograf.tchZlecenieOdczytuTeraz) : tachograf.tchZlecenieOdczytuTeraz != null)
            return false;
        if (tchKlnId != null ? !tchKlnId.equals(tachograf.tchKlnId) : tachograf.tchKlnId != null) return false;
        if (tchAdnId != null ? !tchAdnId.equals(tachograf.tchAdnId) : tachograf.tchAdnId != null) return false;
        if (tchRejestratorSerial != null ? !tchRejestratorSerial.equals(tachograf.tchRejestratorSerial) : tachograf.tchRejestratorSerial != null)
            return false;
        if (tchDataCzasPierwszejPozycji != null ? !tchDataCzasPierwszejPozycji.equals(tachograf.tchDataCzasPierwszejPozycji) : tachograf.tchDataCzasPierwszejPozycji != null)
            return false;
        if (tchDaneTachografuOd != null ? !tchDaneTachografuOd.equals(tachograf.tchDaneTachografuOd) : tachograf.tchDaneTachografuOd != null)
            return false;
        if (tchDaneTachografuDo != null ? !tchDaneTachografuDo.equals(tachograf.tchDaneTachografuDo) : tachograf.tchDaneTachografuDo != null)
            return false;
        if (tchPrzerwijBiezacyOdczyt != null ? !tchPrzerwijBiezacyOdczyt.equals(tachograf.tchPrzerwijBiezacyOdczyt) : tachograf.tchPrzerwijBiezacyOdczyt != null)
            return false;
        if (tchTypZakresOdczytu != null ? !tchTypZakresOdczytu.equals(tachograf.tchTypZakresOdczytu) : tachograf.tchTypZakresOdczytu != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tchId != null ? tchId.hashCode() : 0;
        result = 31 * result + (tchNumerSeryjnyEw != null ? tchNumerSeryjnyEw.hashCode() : 0);
        result = 31 * result + (tchModelEw != null ? tchModelEw.hashCode() : 0);
        result = 31 * result + (tchSymbolEw != null ? tchSymbolEw.hashCode() : 0);
        result = 31 * result + (tchOdtVin != null ? tchOdtVin.hashCode() : 0);
        result = 31 * result + (tchOdtNrRejestracyjny != null ? tchOdtNrRejestracyjny.hashCode() : 0);
        result = 31 * result + (tchOdtModel != null ? tchOdtModel.hashCode() : 0);
        result = 31 * result + (tchDataMontazuEw != null ? tchDataMontazuEw.hashCode() : 0);
        result = 31 * result + (tchOdczytyCoIleDni != null ? tchOdczytyCoIleDni.hashCode() : 0);
        result = 31 * result + (tchOdczytyIleRazyWMiesiacu != null ? tchOdczytyIleRazyWMiesiacu.hashCode() : 0);
        result = 31 * result + (tchOdczytyKtoryTydzien != null ? tchOdczytyKtoryTydzien.hashCode() : 0);
        result = 31 * result + (tchEmailDoOstrzezen != null ? tchEmailDoOstrzezen.hashCode() : 0);
        result = 31 * result + (tchZlecenieOdczytuTeraz != null ? tchZlecenieOdczytuTeraz.hashCode() : 0);
        result = 31 * result + (tchKlnId != null ? tchKlnId.hashCode() : 0);
        result = 31 * result + (tchAdnId != null ? tchAdnId.hashCode() : 0);
        result = 31 * result + (tchRejestratorSerial != null ? tchRejestratorSerial.hashCode() : 0);
        result = 31 * result + (tchDataCzasPierwszejPozycji != null ? tchDataCzasPierwszejPozycji.hashCode() : 0);
        result = 31 * result + (tchDaneTachografuOd != null ? tchDaneTachografuOd.hashCode() : 0);
        result = 31 * result + (tchDaneTachografuDo != null ? tchDaneTachografuDo.hashCode() : 0);
        result = 31 * result + (tchPrzerwijBiezacyOdczyt != null ? tchPrzerwijBiezacyOdczyt.hashCode() : 0);
        result = 31 * result + (tchTypZakresOdczytu != null ? tchTypZakresOdczytu.hashCode() : 0);
        return result;
    }

    @Override
    public Tachograf clone() {
        try {
            return (Tachograf) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
