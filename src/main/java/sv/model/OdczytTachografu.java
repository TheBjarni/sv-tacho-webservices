package sv.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@SequenceGenerator( name = "SEQ_ODCZYT_TACHO", sequenceName = "odczyt_tacho_odt_id_seq")
@Table( name = "odczyt_tachografu")
public class OdczytTachografu implements Cloneable, Serializable {

    private Integer id;
    private Short rodzaj;
    private String vin;
    private String nrRejestracyjny;
    private String modelTachografu;
//    private Integer idKierowcyTacho;
    private Date autoryzacjaKartyP;
    private String stanAutoryzacjiKartaP;
    private Long rejestratorSerial;
    private Integer idTachografu;
    private Date wyslanieZadaniaOdczytu;
    private Date zakonczenieZadaniaOdczytu;
    private Date poczatekDanych;
    private Date koniecDanych;
    private Short iloscStron;
    private Short iloscPowtorzonychStron;
    private Boolean kompletStron;
    private String sciezkaDoPliku;
    private Boolean timeout;
    private Boolean sukces;
    private Date ostatniePobranieDanych;
    private String ostatniPobierajacyDane;
    private Boolean przerwano;
    private String sciezkaDoPlikuK1;
    private String sciezkaDoPlikuK2;
    private String nrKartyPrzedsiebiorstwaDDD;
    private Date dataOdczytuTachoDDD;
    private Date dataOdczytuK1DDD;
    private Date dataOdczytuK2DDD;
    private String vinDDD;
    private String nrRejestracyjnyDDD;
    private Date poczatekDanychDDD;
    private Date koniecDanychDDD;
    private Integer klnId;
    private Boolean przetworzonoTachoDDD;
    private Boolean przetworzonoK1DDD;
    private Boolean przetworzonoK2DDD;
    private String numerSeryjnyTachoDDD;
    private String imieK1DDD;
    private String imieK2DDD;
    private String nazwiskoK1DDD;
    private String nazwiskoK2DDD;
    private String numerKartyK1DDD;
    private String numerKartyK2DDD;

    private Tachograf owner;
    private TachoKierowcy driverOwner;
    private TachoKierowcy driverOwner2;
    private Rejestrator rejestrator;

    @Id
    @Column(name = "odt_id")
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SEQ_ODCZYT_TACHO")
    public Integer getId() {
        return id;
    }

    @Column(name = "odt_rodzaj", insertable = false, updatable = false)
    public Short getRodzaj() {
        return rodzaj;
    }

    @Column(name = "odt_vin")
    public String getVin() {
        return vin;
    }

    @Column(name = "odt_nr_rejestracyjny")
    public String getNrRejestracyjny() {
        return nrRejestracyjny;
    }

    @Column(name = "odt_model_tachografu")
    public String getModelTachografu() {
        return modelTachografu;
    }

//    @Column(name = "odt_tck_id")
//    public Integer getIdKierowcyTacho() {
//        return idKierowcyTacho;
//    }

    @Column(name = "odt_weryfikacja_karty_przedsiebiorstwa")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getAutoryzacjaKartyP() {
        return autoryzacjaKartyP;
    }

    @Column(name = "odt_rejestrator_serial")
    public Long getRejestratorSerial() {
        return rejestratorSerial;
    }

    @Column(name = "odt_tch_id", nullable = false, insertable = false, updatable = false)
    public Integer getIdTachografu() {
        return idTachografu;
    }

    @Column(name = "odt_wyslanie_zadania_odczytu")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWyslanieZadaniaOdczytu() {
        return wyslanieZadaniaOdczytu;
    }

    @Column(name = "odt_zakonczenie_zadania_odczytu")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getZakonczenieZadaniaOdczytu() {
        return zakonczenieZadaniaOdczytu;
    }

    @Column(name = "odt_poczatek_danych")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getPoczatekDanych() {
        return poczatekDanych;
    }

    @Column(name = "odt_koniec_danych")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getKoniecDanych() {
        return koniecDanych;
    }

    @Column(name = "odt_ilosc_stron")
    public Short getIloscStron() {
        return iloscStron;
    }

    @Column(name = "odt_ilosc_powtorzonych_stron")
    public Short getIloscPowtorzonychStron() {
        return iloscPowtorzonychStron;
    }

    @Column(name = "odt_komplet_stron")
    public Boolean getKompletStron() {
        return kompletStron;
    }

    @Column(name = "odt_sciezka_pliku")
    public String getSciezkaDoPliku() {
        return sciezkaDoPliku;
    }

    @Column(name = "odt_timeout")
    public Boolean getTimeout() {
        if (timeout == null) {
            return false;
        }
        return timeout;
    }

    @Column(name = "odt_sukces")
    public Boolean getSukces() {
        if (sukces == null) {
            return false;
        }
        return sukces;
    }

    @Column(name = "odt_ostatnie_pobranie_danych")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOstatniePobranieDanych() {
        return ostatniePobranieDanych;
    }

    @Column(name = "odt_ostatni_pobierajacy_dane")
    public String getOstatniPobierajacyDane() {
        return ostatniPobierajacyDane;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRodzaj(Short rodzaj) {
        this.rodzaj = rodzaj;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public void setNrRejestracyjny(String nrRejestracyjny) {
        this.nrRejestracyjny = nrRejestracyjny;
    }

    public void setModelTachografu(String modelTachografu) {
        this.modelTachografu = modelTachografu;
    }

//    public void setIdKierowcyTacho(Integer idKierowcyTacho) {
//        this.idKierowcyTacho = idKierowcyTacho;
//    }

    public void setAutoryzacjaKartyP(Date autoryzacjaKartyP) {
        this.autoryzacjaKartyP = autoryzacjaKartyP;
    }

    public void setRejestratorSerial(Long rejestratorSerial) {
        this.rejestratorSerial = rejestratorSerial;
    }

    public void setIdTachografu(Integer idTachografu) {
        this.idTachografu = idTachografu;
    }

    public void setWyslanieZadaniaOdczytu(Date wyslanieZadaniaOdczytu) {
        this.wyslanieZadaniaOdczytu = wyslanieZadaniaOdczytu;
    }

    public void setZakonczenieZadaniaOdczytu(Date zakonczenieZadaniaOdczytu) {
        this.zakonczenieZadaniaOdczytu = zakonczenieZadaniaOdczytu;
    }

    public void setPoczatekDanych(Date poczatekDanych) {
        this.poczatekDanych = poczatekDanych;
    }

    public void setKoniecDanych(Date koniecDanych) {
        this.koniecDanych = koniecDanych;
    }

    public void setIloscStron(Short iloscStron) {
        this.iloscStron = iloscStron;
    }

    public void setIloscPowtorzonychStron(Short iloscPowtorzonychStron) {
        this.iloscPowtorzonychStron = iloscPowtorzonychStron;
    }

    public void setKompletStron(Boolean kompletStron) {
        this.kompletStron = kompletStron;
    }

    public void setSciezkaDoPliku(String sciezkaDoPliku) {
        this.sciezkaDoPliku = sciezkaDoPliku;
    }

    public void setTimeout(Boolean timeout) {
        this.timeout = timeout;
    }

    public void setSukces(Boolean sukces) {
        this.sukces = sukces;
    }

    public void setOstatniePobranieDanych(Date ostatniePobranieDanych) {
        this.ostatniePobranieDanych = ostatniePobranieDanych;
    }

    public void setOstatniPobierajacyDane(String ostatniPobierajacyDane) {
        this.ostatniPobierajacyDane = ostatniPobierajacyDane;
    }

    @Column(name = "odt_stan_weryfikacji_karta")
    public String getStanAutoryzacjiKartaP() {
        return stanAutoryzacjiKartaP;
    }

    public void setStanAutoryzacjiKartaP(String stanAutoryzacjiKartaP) {
        this.stanAutoryzacjiKartaP = stanAutoryzacjiKartaP;
    }

    @Column(name = "odt_zlecenie_przerwania")
    public Boolean getPrzerwano() {
        if (przerwano == null) {
            return false;
        }
        return przerwano;
    }

    public void setPrzerwano(Boolean przerwano) {
        this.przerwano = przerwano;
    }

    @Column(name = "odt_sciezka_pliku_k1")
    public String getSciezkaDoPlikuK1() {
        return sciezkaDoPlikuK1;
    }

    public void setSciezkaDoPlikuK1(String sciezkaDoPlikuK1) {
        this.sciezkaDoPlikuK1 = sciezkaDoPlikuK1;
    }

    @Column(name = "odt_sciezka_pliku_k2")
    public String getSciezkaDoPlikuK2() {
        return sciezkaDoPlikuK2;
    }

    public void setSciezkaDoPlikuK2(String sciezkaDoPlikuK2) {
        this.sciezkaDoPlikuK2 = sciezkaDoPlikuK2;
    }

    @Column(name = "odt_nr_karty_przedsiebiorstwa_ddd")
    public String getNrKartyPrzedsiebiorstwaDDD() {
        return nrKartyPrzedsiebiorstwaDDD;
    }

    public void setNrKartyPrzedsiebiorstwaDDD(String nrKartyPrzedsiebiorstwaDDD) {
        this.nrKartyPrzedsiebiorstwaDDD = nrKartyPrzedsiebiorstwaDDD;
    }

    @Column(name = "odt_data_odczytu_tacho_ddd")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDataOdczytuTachoDDD() {
        return dataOdczytuTachoDDD;
    }

    public void setDataOdczytuTachoDDD(Date dataOdczytuTachoDDD) {
        this.dataOdczytuTachoDDD = dataOdczytuTachoDDD;
    }

    @Column(name = "odt_data_odczytu_k1_ddd")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDataOdczytuK1DDD() {
        return dataOdczytuK1DDD;
    }

    public void setDataOdczytuK1DDD(Date dataOdczytuK1DDD) {
        this.dataOdczytuK1DDD = dataOdczytuK1DDD;
    }

    @Column(name = "odt_data_odczytu_k2_ddd")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDataOdczytuK2DDD() {
        return dataOdczytuK2DDD;
    }

    public void setDataOdczytuK2DDD(Date dataOdczytuK2DDD) {
        this.dataOdczytuK2DDD = dataOdczytuK2DDD;
    }

    @Column(name = "odt_vin_ddd")
    public String getVinDDD() {
        return vinDDD;
    }

    public void setVinDDD(String vinDDD) {
        this.vinDDD = vinDDD;
    }

    @Column(name = "odt_nr_rejestracyjny_ddd")
    public String getNrRejestracyjnyDDD() {
        return nrRejestracyjnyDDD;
    }

    public void setNrRejestracyjnyDDD(String nrRejestracyjnyDDD) {
        this.nrRejestracyjnyDDD = nrRejestracyjnyDDD;
    }

    @Column(name = "odt_poczatek_danych_ddd")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getPoczatekDanychDDD() {
        return poczatekDanychDDD;
    }

    public void setPoczatekDanychDDD(Date poczatekDanychDDD) {
        this.poczatekDanychDDD = poczatekDanychDDD;
    }

    @Column(name = "odt_koniec_danych_ddd")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getKoniecDanychDDD() {
        return koniecDanychDDD;
    }

    public void setKoniecDanychDDD(Date koniecDanychDDD) {
        this.koniecDanychDDD = koniecDanychDDD;
    }

    @Column(name = "odt_kln_id")
    public Integer getKlnId() {
        return klnId;
    }

    public void setKlnId(Integer klnId) {
        this.klnId = klnId;
    }

    @Column(name = "odt_przetworzono_ddd_tacho")
    public Boolean getPrzetworzonoTachoDDD() {
        return przetworzonoTachoDDD;
    }

    public void setPrzetworzonoTachoDDD(Boolean przetworzonoTachoDDD) {
        this.przetworzonoTachoDDD = przetworzonoTachoDDD;
    }

    @Column(name = "odt_przetworzono_ddd_k1")
    public Boolean getPrzetworzonoK1DDD() {
        return przetworzonoK1DDD;
    }

    public void setPrzetworzonoK1DDD(Boolean przetworzonoK1DDD) {
        this.przetworzonoK1DDD = przetworzonoK1DDD;
    }

    @Column(name = "odt_przetworzono_ddd_k2")
    public Boolean getPrzetworzonoK2DDD() {
        return przetworzonoK2DDD;
    }

    public void setPrzetworzonoK2DDD(Boolean przetworzonoK2DDD) {
        this.przetworzonoK2DDD = przetworzonoK2DDD;
    }

    @Column(name = "odt_numer_seryjny_tachografu_ddd")
    public String getNumerSeryjnyTachoDDD() {
        return numerSeryjnyTachoDDD;
    }

    public void setNumerSeryjnyTachoDDD(String numerSeryjnyTachoDDD) {
        this.numerSeryjnyTachoDDD = numerSeryjnyTachoDDD;
    }

    @Column(name = "odt_imie_k1_ddd")
    public String getImieK1DDD() {
        return imieK1DDD;
    }

    public void setImieK1DDD(String imieK1DDD) {
        this.imieK1DDD = imieK1DDD;
    }

    @Column(name = "odt_imie_k2_ddd")
    public String getImieK2DDD() {
        return imieK2DDD;
    }

    public void setImieK2DDD(String imieK2DDD) {
        this.imieK2DDD = imieK2DDD;
    }

    @Column(name = "odt_nazwisko_k1_ddd")
    public String getNazwiskoK1DDD() {
        return nazwiskoK1DDD;
    }

    public void setNazwiskoK1DDD(String nazwiskoK1DDD) {
        this.nazwiskoK1DDD = nazwiskoK1DDD;
    }

    @Column(name = "odt_nazwisko_k2_ddd")
    public String getNazwiskoK2DDD() {
        return nazwiskoK2DDD;
    }

    public void setNazwiskoK2DDD(String nazwiskoK2DDD) {
        this.nazwiskoK2DDD = nazwiskoK2DDD;
    }

    @Column(name = "odt_numer_karty_k1_ddd")
    public String getNumerKartyK1DDD() {
        return numerKartyK1DDD;
    }

    public void setNumerKartyK1DDD(String numerKartyK1DDD) {
        this.numerKartyK1DDD = numerKartyK1DDD;
    }

    @Column(name = "odt_numer_karty_k2_ddd")
    public String getNumerKartyK2DDD() {
        return numerKartyK2DDD;
    }

    public void setNumerKartyK2DDD(String numerKartyK2DDD) {
        this.numerKartyK2DDD = numerKartyK2DDD;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="odt_tch_id")
    public Tachograf getOwner() {
        return owner;
    }

    public void setOwner(Tachograf owner) {
        this.owner = owner;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="odt_tck_id")
    public TachoKierowcy getDriverOwner() {
        return driverOwner;
    }

    public void setDriverOwner(TachoKierowcy driverOwner) {
        this.driverOwner = driverOwner;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="odt_tck2_id")
    public TachoKierowcy getDriverOwner2() {
        return driverOwner2;
    }

    public void setDriverOwner2(TachoKierowcy driverOwner2) {
        this.driverOwner2 = driverOwner2;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="odt_rejestrator_serial", nullable = false, insertable = false, updatable = false)
    public Rejestrator getRejestrator() {
        return rejestrator;
    }

    public void setRejestrator(Rejestrator rejestrator) {
        this.rejestrator = rejestrator;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

