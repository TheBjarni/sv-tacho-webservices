package sv.model;

import javax.persistence.*;
@Entity
@Table(name = "auto_dane", schema = "public")
@SequenceGenerator(name = "ADSEQ", sequenceName = "auto_dane_adn_id_seq")
public class AutoDane implements java.io.Serializable, Comparable<AutoDane> {

    private Long adnId;
    private Klient klient;
    private String adnNrRejestracyjny;
    private String adnMarka;
    private String adnModel;
    private Boolean adnAktywny;
    private Boolean audytSt;
    private String symbol;

    @Id
    @Column(name = "adn_id", unique = true, nullable = false)
    @GeneratedValue(generator = "ADSEQ", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return this.adnId;
    }

    public void setId(Long adnId) {
        this.adnId = adnId;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "adn_kln_id", nullable = false)
    public Klient getKlient() {
        return this.klient;
    }

    public void setKlient(Klient klient) {
        this.klient = klient;
    }

    @Column(name = "adn_nr_rejestracyjny", nullable = false, length = 25)
    public String getNrRejestracyjny() {
        return this.adnNrRejestracyjny;
    }

    public void setNrRejestracyjny(String adnNrRejestracyjny) {
        this.adnNrRejestracyjny = adnNrRejestracyjny;
    }

    @Column(name = "adn_marka", length = 25)
    public String getMarka() {
        return this.adnMarka;
    }

    public void setMarka(String adnMarka) {
        this.adnMarka = adnMarka;
    }

    @Column(name = "adn_model", length = 25)
    public String getModel() {
        return this.adnModel;
    }

    public void setModel(String adnModel) {
        this.adnModel = adnModel;
    }

    @Column(name = "adn_aktywny")
    public Boolean getAktywny() {
        return this.adnAktywny;
    }

    public void setAktywny(Boolean adnAktywny) {
        this.adnAktywny = adnAktywny;
    }

    @Column(name = "adn_symbol", length = 128)
    public String getSymbol() {
        return this.symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Column(name = "adn_audyt_st")
    public Boolean getAudytSt() {
        return audytSt;
    }

    public void setAudytSt(Boolean audytSt) {
        this.audytSt = audytSt;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adnId == null) ? 0 : adnId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AutoDane other = (AutoDane) obj;
        if (adnId == null) {
            if (other.adnId != null)
                return false;
        } else if (!adnId.equals(other.adnId))
            return false;
        return true;
    }

    @Override
    public int compareTo(AutoDane o) {

        return getId().compareTo(o.getId());
    }

    public Long id() {
        return getId();
    }
}
