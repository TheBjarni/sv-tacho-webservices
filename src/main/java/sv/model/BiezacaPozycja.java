package sv.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by tymek on 04.05.17.
 */
@Entity
@Table(name = "biezaca_pozycja")
public class BiezacaPozycja {
    private Long bpzId;
    private Integer bpzKierowcaId;
    private Timestamp bpzCzas;
    private Integer bpzPredkosc;
    private Integer bpzRejestratorSerial;
    private Integer bpzPrzebieg;
    private Long bpzAdnId;
    private String bpzKraj;
    private String bpzWojewodztwo;
    private String bpzPowiat;
    private String bpzGmina;
    private String bpzDroga;
    private String bpzMiejscowosc;
    private String bpzKodPocztowy;
    private String bpzUlica;
    private BiezacaPozycjaTacho tachoPosition;

    private AutoDane auto;

    @Id
    @Column(name = "bpz_id")
    public Long getBpzId() {
        return bpzId;
    }

    public void setBpzId(Long bpzId) {
        this.bpzId = bpzId;
    }

    @Basic
    @Column(name = "bpz_kierowca_id")
    public Integer getBpzKierowcaId() {
        return bpzKierowcaId;
    }

    public void setBpzKierowcaId(Integer bpzKierowcaId) {
        this.bpzKierowcaId = bpzKierowcaId;
    }

    @Basic
    @Column(name = "bpz_czas")
    public Timestamp getBpzCzas() {
        return bpzCzas;
    }

    public void setBpzCzas(Timestamp bpzCzas) {
        this.bpzCzas = bpzCzas;
    }

    @Basic
    @Column(name = "bpz_predkosc")
    public Integer getBpzPredkosc() {
        return bpzPredkosc;
    }

    public void setBpzPredkosc(Integer bpzPredkosc) {
        this.bpzPredkosc = bpzPredkosc;
    }

    @Basic
    @Column(name = "bpz_rejestrator_serial")
    public Integer getBpzRejestratorSerial() {
        return bpzRejestratorSerial;
    }

    public void setBpzRejestratorSerial(Integer bpzRejestratorSerial) {
        this.bpzRejestratorSerial = bpzRejestratorSerial;
    }

    @Basic
    @Column(name = "bpz_przebieg")
    public Integer getBpzPrzebieg() {
        return bpzPrzebieg;
    }

    public void setBpzPrzebieg(Integer bpzPrzebieg) {
        this.bpzPrzebieg = bpzPrzebieg;
    }

    @Basic
    @Column(name = "bpz_adn_id")
    public Long getBpzAdnId() {
        return bpzAdnId;
    }

    public void setBpzAdnId(Long bpzAdnId) {
        this.bpzAdnId = bpzAdnId;
    }

    @Basic
    @Column(name = "bpz_kraj")
    public String getBpzKraj() {
        return bpzKraj;
    }

    public void setBpzKraj(String bpzKraj) {
        this.bpzKraj = bpzKraj;
    }

    @Basic
    @Column(name = "bpz_wojewodztwo")
    public String getBpzWojewodztwo() {
        return bpzWojewodztwo;
    }

    public void setBpzWojewodztwo(String bpzWojewodztwo) {
        this.bpzWojewodztwo = bpzWojewodztwo;
    }

    @Basic
    @Column(name = "bpz_powiat")
    public String getBpzPowiat() {
        return bpzPowiat;
    }

    public void setBpzPowiat(String bpzPowiat) {
        this.bpzPowiat = bpzPowiat;
    }

    @Basic
    @Column(name = "bpz_gmina")
    public String getBpzGmina() {
        return bpzGmina;
    }

    public void setBpzGmina(String bpzGmina) {
        this.bpzGmina = bpzGmina;
    }

    @Basic
    @Column(name = "bpz_droga")
    public String getBpzDroga() {
        return bpzDroga;
    }

    public void setBpzDroga(String bpzDroga) {
        this.bpzDroga = bpzDroga;
    }

    @Basic
    @Column(name = "bpz_miejscowosc")
    public String getBpzMiejscowosc() {
        return bpzMiejscowosc;
    }

    public void setBpzMiejscowosc(String bpzMiejscowosc) {
        this.bpzMiejscowosc = bpzMiejscowosc;
    }

    @Basic
    @Column(name = "bpz_kod_pocztowy")
    public String getBpzKodPocztowy() {
        return bpzKodPocztowy;
    }

    public void setBpzKodPocztowy(String bpzKodPocztowy) {
        this.bpzKodPocztowy = bpzKodPocztowy;
    }

    @Basic
    @Column(name = "bpz_ulica")
    public String getBpzUlica() {
        return bpzUlica;
    }

    public void setBpzUlica(String bpzUlica) {
        this.bpzUlica = bpzUlica;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bpz_id", nullable = false, insertable = false, updatable = false)
    public BiezacaPozycjaTacho getTachoPosition() {
        return tachoPosition;
    }

    public void setTachoPosition(BiezacaPozycjaTacho tachoPosition) {
        this.tachoPosition = tachoPosition;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bpz_adn_id", nullable = false, insertable = false, updatable = false)
    public AutoDane getAuto() {
        return auto;
    }

    public void setAuto(AutoDane auto) {
        this.auto = auto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BiezacaPozycja that = (BiezacaPozycja) o;

        if (bpzId != null ? !bpzId.equals(that.bpzId) : that.bpzId != null) return false;
        if (bpzKierowcaId != null ? !bpzKierowcaId.equals(that.bpzKierowcaId) : that.bpzKierowcaId != null)
            return false;
        if (bpzCzas != null ? !bpzCzas.equals(that.bpzCzas) : that.bpzCzas != null) return false;
        if (bpzPredkosc != null ? !bpzPredkosc.equals(that.bpzPredkosc) : that.bpzPredkosc != null) return false;
        if (bpzRejestratorSerial != null ? !bpzRejestratorSerial.equals(that.bpzRejestratorSerial) : that.bpzRejestratorSerial != null)
            return false;
        if (bpzPrzebieg != null ? !bpzPrzebieg.equals(that.bpzPrzebieg) : that.bpzPrzebieg != null) return false;
        if (bpzAdnId != null ? !bpzAdnId.equals(that.bpzAdnId) : that.bpzAdnId != null) return false;
        if (bpzKraj != null ? !bpzKraj.equals(that.bpzKraj) : that.bpzKraj != null) return false;
        if (bpzWojewodztwo != null ? !bpzWojewodztwo.equals(that.bpzWojewodztwo) : that.bpzWojewodztwo != null) return false;
        if (bpzPowiat != null ? !bpzPowiat.equals(that.bpzPowiat) : that.bpzPowiat != null) return false;
        if (bpzGmina != null ? !bpzGmina.equals(that.bpzGmina) : that.bpzGmina != null) return false;
        if (bpzDroga != null ? !bpzDroga.equals(that.bpzDroga) : that.bpzDroga != null) return false;
        if (bpzMiejscowosc != null ? !bpzMiejscowosc.equals(that.bpzMiejscowosc) : that.bpzMiejscowosc != null) return false;
        if (bpzKodPocztowy != null ? !bpzKodPocztowy.equals(that.bpzKodPocztowy) : that.bpzKodPocztowy != null) return false;
        if (bpzUlica != null ? !bpzUlica.equals(that.bpzUlica) : that.bpzUlica != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = bpzId != null ? bpzId.hashCode() : 0;
        result = 31 * result + (bpzKierowcaId != null ? bpzKierowcaId.hashCode() : 0);
        result = 31 * result + (bpzCzas != null ? bpzCzas.hashCode() : 0);
        result = 31 * result + (bpzPredkosc != null ? bpzPredkosc.hashCode() : 0);
        result = 31 * result + (bpzRejestratorSerial != null ? bpzRejestratorSerial.hashCode() : 0);
        result = 31 * result + (bpzPrzebieg != null ? bpzPrzebieg.hashCode() : 0);
        result = 31 * result + (bpzAdnId != null ? bpzAdnId.hashCode() : 0);
        result = 31 * result + (bpzKraj != null ? bpzKraj.hashCode() : 0);
        result = 31 * result + (bpzWojewodztwo != null ? bpzWojewodztwo.hashCode() : 0);
        result = 31 * result + (bpzPowiat != null ? bpzPowiat.hashCode() : 0);
        result = 31 * result + (bpzGmina != null ? bpzGmina.hashCode() : 0);
        result = 31 * result + (bpzDroga != null ? bpzDroga.hashCode() : 0);
        result = 31 * result + (bpzMiejscowosc != null ? bpzMiejscowosc.hashCode() : 0);
        result = 31 * result + (bpzKodPocztowy != null ? bpzKodPocztowy.hashCode() : 0);
        result = 31 * result + (bpzUlica != null ? bpzUlica.hashCode() : 0);
        return result;
    }
}
