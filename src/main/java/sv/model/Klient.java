package sv.model;

import javax.persistence.*;

/**
 * Created by tymek on 16.03.17.
 */
@Entity
@SequenceGenerator( name = "SEQ_KLIENT", sequenceName = "klient_kln_id_seq")
@Table(name = "klient")
public class Klient {
    private Integer id;

    @Id
    @Column(name = "kln_id")
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SEQ_KLIENT")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
