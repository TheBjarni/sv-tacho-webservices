package sv.model;

import javax.persistence.*;

/**
 * Created by tymek on 16.03.17.
 */
@Entity
@SequenceGenerator( name = "SEQ_UZYTKOWNIK", sequenceName = "uzytkownik_uzt_id_seq")
@Table(name = "uzytkownik")
public class Uzytkownik {
    Integer id;
    Integer prcId;
    String login;
    String haslo;
    Long rlaId;

    @Id
    @Column(name = "uzt_id")
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SEQ_UZYTKOWNIK")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "uzt_prc_id")
    public Integer getPrcId() {
        return prcId;
    }

    public void setPrcId(Integer prcId) {
        this.prcId = prcId;
    }

    @Column(name = "uzt_login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Column(name = "uzt_haslo")
    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    @Column(name = "uzt_rla_id")
    public Long getRlaId() {
        return rlaId;
    }

    public void setRlaId(Long rlaId) {
        this.rlaId = rlaId;
    }
}