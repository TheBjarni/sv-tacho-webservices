package sv.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by tymek on 05.04.17.
 */
@Entity
@Table(name = "tacho_kierowcy")
public class TachoKierowcy {
    private Integer tckId;
    private String tckImie;
    private String tckNazwisko;
    private String tckNumerKarty;
    private Timestamp tckAudytDt;
    private Timestamp tckOstatniaObecnosc;
    private Timestamp tckOstatniUdanyOdczyt;
    private Timestamp tckOstatniaProbaOdczytu;
    private Timestamp tckOdczytajTeraz;
    private Integer tckTchId;
    private Pracownik pracownik;
    private List<OdczytTachografu> odczyty;
    private String nrRejOstatniegoPojazdu;
    private Long czasOstatniejPozycji;
    private String nrRejOstatniejPozycji;
    private String tckImieDDD;
    private String tckNazwiskoDDD;
    private String numerKartyDDD;
    private Long tckOstatniPojazd;
    private AutoDane ostatniPojazd;
    private BiezacaPozycja ostatniaPozycja;

    private Date dataOstatniegoOdczytu;

    @Id
    @Column(name = "tck_id")
    public Integer getTckId() {
        return tckId;
    }

    public void setTckId(Integer tckId) {
        this.tckId = tckId;
    }

    @Basic
    @Column(name = "tck_imie")
    public String getTckImie() {
        return tckImie;
    }

    public void setTckImie(String tckImie) {
        this.tckImie = tckImie;
    }

    @Basic
    @Column(name = "tck_nazwisko")
    public String getTckNazwisko() {
        return tckNazwisko;
    }

    public void setTckNazwisko(String tckNazwisko) {
        this.tckNazwisko = tckNazwisko;
    }

    @Basic
    @Column(name = "tck_numer_karty")
    public String getTckNumerKarty() {
        return tckNumerKarty;
    }

    public void setTckNumerKarty(String tckNumerKarty) {
        this.tckNumerKarty = tckNumerKarty;
    }

    @Basic
    @Column(name = "tck_audyt_dt")
    public Timestamp getTckAudytDt() {
        return tckAudytDt;
    }

    public void setTckAudytDt(Timestamp tckAudytDt) {
        this.tckAudytDt = tckAudytDt;
    }

    @Basic
    @Column(name = "tck_ostatnia_obecnosc")
    public Timestamp getTckOstatniaObecnosc() {
        return tckOstatniaObecnosc;
    }

    public void setTckOstatniaObecnosc(Timestamp tckOstatniaObecnosc) {
        this.tckOstatniaObecnosc = tckOstatniaObecnosc;
    }

    @Basic
    @Column(name = "tck_ostatni_udany_odczyt")
    public Timestamp getTckOstatniUdanyOdczyt() {
        return tckOstatniUdanyOdczyt;
    }

    public void setTckOstatniUdanyOdczyt(Timestamp tckOstatniUdanyOdczyt) {
        this.tckOstatniUdanyOdczyt = tckOstatniUdanyOdczyt;
    }

    @Basic
    @Column(name = "tck_ostatnia_proba_odczytu")
    public Timestamp getTckOstatniaProbaOdczytu() {
        return tckOstatniaProbaOdczytu;
    }

    public void setTckOstatniaProbaOdczytu(Timestamp tckOstatniaProbaOdczytu) {
        this.tckOstatniaProbaOdczytu = tckOstatniaProbaOdczytu;
    }

    @Basic
    @Column(name = "tck_odczytaj_teraz")
    public Timestamp getTckOdczytajTeraz() {
        return tckOdczytajTeraz;
    }

    public void setTckOdczytajTeraz(Timestamp tckOdczytajTeraz) {
        this.tckOdczytajTeraz = tckOdczytajTeraz;
    }

    @Basic
    @Column(name = "tck_tch_id")
    public Integer getTckTchId() {
        return tckTchId;
    }

    public void setTckTchId(Integer tckTchId) {
        this.tckTchId = tckTchId;
    }

    @OneToMany(mappedBy = "driverOwner")
    public List<OdczytTachografu> getOdczyty() {
        return odczyty;
    }

    public void setOdczyty(List<OdczytTachografu> odczyty) {
        this.odczyty = odczyty;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tck_prc_id")
    public Pracownik getPracownik() {
        return pracownik;
    }

    public void setPracownik(Pracownik pracownik) {
        this.pracownik = pracownik;
    }

    @Transient
    public String getNrRejOstatniegoPojazdu() {
        return nrRejOstatniegoPojazdu;
    }

    public void setNrRejOstatniegoPojazdu(String nrRejOstatniegoPojazdu) {
        this.nrRejOstatniegoPojazdu = nrRejOstatniegoPojazdu;
    }

    @Transient
    public Long getCzasOstatniejPozycji() {
        return czasOstatniejPozycji;
    }

    public void setCzasOstatniejPozycji(Long czasOstatniejPozycji) {
        this.czasOstatniejPozycji = czasOstatniejPozycji;
    }

    @Transient
    public String getNrRejOstatniejPozycji() {
        return nrRejOstatniejPozycji;
    }

    public void setNrRejOstatniejPozycji(String nrRejOstatniejPozycji) {
        this.nrRejOstatniejPozycji = nrRejOstatniejPozycji;
    }

    @Column(name = "tck_imie_ddd")
    public String getTckImieDDD() {
        return tckImieDDD;
    }

    public void setTckImieDDD(String tckImieDDD) {
        this.tckImieDDD = tckImieDDD;
    }

    @Column(name = "tck_nazwisko_ddd")
    public String getTckNazwiskoDDD() {
        return tckNazwiskoDDD;
    }

    public void setTckNazwiskoDDD(String tckNazwiskoDDD) {
        this.tckNazwiskoDDD = tckNazwiskoDDD;
    }

    @Column(name = "tck_numer_karty_ddd")
    public String getNumerKartyDDD() {
        return numerKartyDDD;
    }

    public void setNumerKartyDDD(String numerKartyDDD) {
        this.numerKartyDDD = numerKartyDDD;
    }

    @Column(name = "tck_ostatni_adn_id")
    public Long getTckOstatniPojazd() {
        return tckOstatniPojazd;
    }

    public void setTckOstatniPojazd(Long tckOstatniPojazd) {
        this.tckOstatniPojazd = tckOstatniPojazd;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tck_ostatni_adn_id", nullable = false, insertable = false, updatable = false)
    public AutoDane getOstatniPojazd() {
        return ostatniPojazd;
    }

    public void setOstatniPojazd(AutoDane ostatniPojazd) {
        this.ostatniPojazd = ostatniPojazd;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tck_ostatnia_bpz_id", nullable = false, insertable = false, updatable = false)
    public BiezacaPozycja getOstatniaPozycja() {
        return ostatniaPozycja;
    }

    public void setOstatniaPozycja(BiezacaPozycja ostatniaPozycja) {
        this.ostatniaPozycja = ostatniaPozycja;
    }

    @Transient
    public Date getDataOstatniegoOdczytu() {
        return dataOstatniegoOdczytu;
    }

    public void setDataOstatniegoOdczytu(Date dataOstatniegoOdczytu) {
        this.dataOstatniegoOdczytu = dataOstatniegoOdczytu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TachoKierowcy that = (TachoKierowcy) o;

        if (tckId != null ? !tckId.equals(that.tckId) : that.tckId != null) return false;
        if (tckImie != null ? !tckImie.equals(that.tckImie) : that.tckImie != null) return false;
        if (tckNazwisko != null ? !tckNazwisko.equals(that.tckNazwisko) : that.tckNazwisko != null) return false;
        if (tckNumerKarty != null ? !tckNumerKarty.equals(that.tckNumerKarty) : that.tckNumerKarty != null)
            return false;
        if (tckAudytDt != null ? !tckAudytDt.equals(that.tckAudytDt) : that.tckAudytDt != null) return false;
        if (tckOstatniaObecnosc != null ? !tckOstatniaObecnosc.equals(that.tckOstatniaObecnosc) : that.tckOstatniaObecnosc != null)
            return false;
        if (tckOstatniUdanyOdczyt != null ? !tckOstatniUdanyOdczyt.equals(that.tckOstatniUdanyOdczyt) : that.tckOstatniUdanyOdczyt != null)
            return false;
        if (tckOstatniaProbaOdczytu != null ? !tckOstatniaProbaOdczytu.equals(that.tckOstatniaProbaOdczytu) : that.tckOstatniaProbaOdczytu != null)
            return false;
        if (tckOdczytajTeraz != null ? !tckOdczytajTeraz.equals(that.tckOdczytajTeraz) : that.tckOdczytajTeraz != null)
            return false;
        if (tckTchId != null ? !tckTchId.equals(that.tckTchId) : that.tckTchId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tckId != null ? tckId.hashCode() : 0;
        result = 31 * result + (tckImie != null ? tckImie.hashCode() : 0);
        result = 31 * result + (tckNazwisko != null ? tckNazwisko.hashCode() : 0);
        result = 31 * result + (tckNumerKarty != null ? tckNumerKarty.hashCode() : 0);
        result = 31 * result + (tckAudytDt != null ? tckAudytDt.hashCode() : 0);
        result = 31 * result + (tckOstatniaObecnosc != null ? tckOstatniaObecnosc.hashCode() : 0);
        result = 31 * result + (tckOstatniUdanyOdczyt != null ? tckOstatniUdanyOdczyt.hashCode() : 0);
        result = 31 * result + (tckOstatniaProbaOdczytu != null ? tckOstatniaProbaOdczytu.hashCode() : 0);
        result = 31 * result + (tckOdczytajTeraz != null ? tckOdczytajTeraz.hashCode() : 0);
        result = 31 * result + (tckTchId != null ? tckTchId.hashCode() : 0);
        return result;
    }
}
