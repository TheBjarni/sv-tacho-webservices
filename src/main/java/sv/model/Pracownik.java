package sv.model;

import javax.persistence.*;

/**
 * Created by tymek on 16.03.17.
 */
@Entity
@SequenceGenerator( name = "SEQ_PRACOWNIK", sequenceName = "pracownik_prc_id_seq")
@Table(name = "pracownik")
public class Pracownik {
    Integer id;
    Integer klnId;
    String numerKartyKierowcy;

    @Id
    @Column(name = "prc_id")
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SEQ_PRACOWNIK")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "prc_kln_id")
    public Integer getKlnId() {
        return klnId;
    }

    public void setKlnId(Integer klnId) {
        this.klnId = klnId;
    }

    @Column(name = "prc_numer_karty_kierowcy")
    public String getNumerKartyKierowcy() {
        return numerKartyKierowcy;
    }

    public void setNumerKartyKierowcy(String numerKartyKierowcy) {
        this.numerKartyKierowcy = numerKartyKierowcy;
    }
}
