package sv.model;

import javax.persistence.*;

/**
 * Created by tymek on 23.03.17.
 */

@Entity
@Table(name = "rejestrator")
public class Rejestrator {
    private Long rejestratorSerial;
    private Long klnId;
    private AutoDane car;
    private BiezacaPozycja lastPosition;
    private String regNr;

    @Id
    @Column(name = "aut_rejestrator_serial")
    public Long getRejestratorSerial() {
        return rejestratorSerial;
    }

    public void setRejestratorSerial(Long rejestratorSerial) {
        this.rejestratorSerial = rejestratorSerial;
    }

    @Column(name = "aut_kln_id")
    public Long getKlnId() {
        return klnId;
    }

    public void setKlnId(Long klnId) {
        this.klnId = klnId;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aut_adn_id")
    public AutoDane getCar() {
        return car;
    }

    public void setCar(AutoDane car) {
        this.car = car;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ost_bpz_id")
    public BiezacaPozycja getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(BiezacaPozycja lastPosition) {
        this.lastPosition = lastPosition;
    }

    @Column(name = "aut_nr_rej")
    public String getRegNr() {
        return regNr;
    }

    public void setRegNr(String regNr) {
        this.regNr = regNr;
    }
}
