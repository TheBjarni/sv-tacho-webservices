package sv.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by tymek on 27.04.17.
 */
@Entity
@Table(name = "uprawnienie")
public class Uprawnienie {
    private Integer uprId;
    private String uprNazwa;
    private Long uprUprId;
    private Boolean uprSuperUprawnienie;
    private Boolean uprAudytSt;
    private Long uprAudytUt;
    private Timestamp uprAudytDt;
    private Long uprAudytUm;
    private Timestamp uprAudytDm;
    private List<Rola> role;

    @Id
    @Column(name = "upr_id")
    public Integer getUprId() {
        return uprId;
    }

    public void setUprId(Integer uprId) {
        this.uprId = uprId;
    }

    @Basic
    @Column(name = "upr_nazwa")
    public String getUprNazwa() {
        return uprNazwa;
    }

    public void setUprNazwa(String uprNazwa) {
        this.uprNazwa = uprNazwa;
    }

    @Basic
    @Column(name = "upr_upr_id")
    public Long getUprUprId() {
        return uprUprId;
    }

    public void setUprUprId(Long uprUprId) {
        this.uprUprId = uprUprId;
    }

    @Basic
    @Column(name = "upr_super_uprawnienie")
    public Boolean getUprSuperUprawnienie() {
        return uprSuperUprawnienie;
    }

    public void setUprSuperUprawnienie(Boolean uprSuperUprawnienie) {
        this.uprSuperUprawnienie = uprSuperUprawnienie;
    }

    @Basic
    @Column(name = "upr_audyt_st")
    public Boolean getUprAudytSt() {
        return uprAudytSt;
    }

    public void setUprAudytSt(Boolean uprAudytSt) {
        this.uprAudytSt = uprAudytSt;
    }

    @Basic
    @Column(name = "upr_audyt_ut")
    public Long getUprAudytUt() {
        return uprAudytUt;
    }

    public void setUprAudytUt(Long uprAudytUt) {
        this.uprAudytUt = uprAudytUt;
    }

    @Basic
    @Column(name = "upr_audyt_dt")
    public Timestamp getUprAudytDt() {
        return uprAudytDt;
    }

    public void setUprAudytDt(Timestamp uprAudytDt) {
        this.uprAudytDt = uprAudytDt;
    }

    @Basic
    @Column(name = "upr_audyt_um")
    public Long getUprAudytUm() {
        return uprAudytUm;
    }

    public void setUprAudytUm(Long uprAudytUm) {
        this.uprAudytUm = uprAudytUm;
    }

    @Basic
    @Column(name = "upr_audyt_dm")
    public Timestamp getUprAudytDm() {
        return uprAudytDm;
    }

    public void setUprAudytDm(Timestamp uprAudytDm) {
        this.uprAudytDm = uprAudytDm;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "uprawnienia")
    public List<Rola> getRole() {
        return role;
    }

    public void setRole(List<Rola> role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Uprawnienie that = (Uprawnienie) o;

        if (uprId != null ? !uprId.equals(that.uprId) : that.uprId != null) return false;
        if (uprNazwa != null ? !uprNazwa.equals(that.uprNazwa) : that.uprNazwa != null) return false;
        if (uprUprId != null ? !uprUprId.equals(that.uprUprId) : that.uprUprId != null) return false;
        if (uprSuperUprawnienie != null ? !uprSuperUprawnienie.equals(that.uprSuperUprawnienie) : that.uprSuperUprawnienie != null)
            return false;
        if (uprAudytSt != null ? !uprAudytSt.equals(that.uprAudytSt) : that.uprAudytSt != null) return false;
        if (uprAudytUt != null ? !uprAudytUt.equals(that.uprAudytUt) : that.uprAudytUt != null) return false;
        if (uprAudytDt != null ? !uprAudytDt.equals(that.uprAudytDt) : that.uprAudytDt != null) return false;
        if (uprAudytUm != null ? !uprAudytUm.equals(that.uprAudytUm) : that.uprAudytUm != null) return false;
        if (uprAudytDm != null ? !uprAudytDm.equals(that.uprAudytDm) : that.uprAudytDm != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uprId != null ? uprId.hashCode() : 0;
        result = 31 * result + (uprNazwa != null ? uprNazwa.hashCode() : 0);
        result = 31 * result + (uprUprId != null ? uprUprId.hashCode() : 0);
        result = 31 * result + (uprSuperUprawnienie != null ? uprSuperUprawnienie.hashCode() : 0);
        result = 31 * result + (uprAudytSt != null ? uprAudytSt.hashCode() : 0);
        result = 31 * result + (uprAudytUt != null ? uprAudytUt.hashCode() : 0);
        result = 31 * result + (uprAudytDt != null ? uprAudytDt.hashCode() : 0);
        result = 31 * result + (uprAudytUm != null ? uprAudytUm.hashCode() : 0);
        result = 31 * result + (uprAudytDm != null ? uprAudytDm.hashCode() : 0);
        return result;
    }
}
