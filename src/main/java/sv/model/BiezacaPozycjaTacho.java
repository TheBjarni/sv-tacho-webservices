package sv.model;

import javax.persistence.*;

/**
 * Created by tymek on 04.05.17.
 */
@Entity
@Table(name = "biezaca_pozycja_tacho")
public class BiezacaPozycjaTacho {
    private Long bpztId;
    private Integer bpztPredkosc;
    private Long bpztPrzebieg;
    private Boolean bpztSilnik;
    private Short bpztKierunekJazdy;
    private Short bpztObecneKartyKierowcow;
    private String bpztNumerKartyK1;
    private Short bpztLimitCzasuK1;
    private Short bpztTrybPracyK1;
    private Integer bpztCzasPracyPrzerwa45K1;
    private Integer bpztCalkowityCzasPrzerwK1;
    private Integer bpztCzasAktywnosciK1;
    private Integer bpztCzasJazd2TygodnieK1;
    private String bpztNumerKartyK2;
    private Short bpztLimitCzasuK2;
    private Short bpztTrybPracyK2;
    private Integer bpztCzasPracyPrzerwa45K2;
    private Integer bpztCalkowityCzasPrzerwK2;
    private Integer bpztCzasAktywnosciK2;
    private Integer bpztCzasJazd2TygodnieK2;
    private Short bpztDaneDodatkowe;

    @Id
    @Column(name = "bpzt_id")
    public Long getBpztId() {
        return bpztId;
    }

    public void setBpztId(Long bpztId) {
        this.bpztId = bpztId;
    }

    @Basic
    @Column(name = "bpzt_predkosc")
    public Integer getBpztPredkosc() {
        return bpztPredkosc;
    }

    public void setBpztPredkosc(Integer bpztPredkosc) {
        this.bpztPredkosc = bpztPredkosc;
    }

    @Basic
    @Column(name = "bpzt_przebieg")
    public Long getBpztPrzebieg() {
        return bpztPrzebieg;
    }

    public void setBpztPrzebieg(Long bpztPrzebieg) {
        this.bpztPrzebieg = bpztPrzebieg;
    }

    @Basic
    @Column(name = "bpzt_silnik")
    public Boolean getBpztSilnik() {
        return bpztSilnik;
    }

    public void setBpztSilnik(Boolean bpztSilnik) {
        this.bpztSilnik = bpztSilnik;
    }

    @Basic
    @Column(name = "bpzt_kierunek_jazdy")
    public Short getBpztKierunekJazdy() {
        return bpztKierunekJazdy;
    }

    public void setBpztKierunekJazdy(Short bpztKierunekJazdy) {
        this.bpztKierunekJazdy = bpztKierunekJazdy;
    }

    @Basic
    @Column(name = "bpzt_obecne_karty_kierowcow")
    public Short getBpztObecneKartyKierowcow() {
        return bpztObecneKartyKierowcow;
    }

    public void setBpztObecneKartyKierowcow(Short bpztObecneKartyKierowcow) {
        this.bpztObecneKartyKierowcow = bpztObecneKartyKierowcow;
    }

    @Basic
    @Column(name = "bpzt_numer_karty_k1")
    public String getBpztNumerKartyK1() {
        return bpztNumerKartyK1;
    }

    public void setBpztNumerKartyK1(String bpztNumerKartyK1) {
        this.bpztNumerKartyK1 = bpztNumerKartyK1;
    }

    @Basic
    @Column(name = "bpzt_limit_czasu_k1")
    public Short getBpztLimitCzasuK1() {
        return bpztLimitCzasuK1;
    }

    public void setBpztLimitCzasuK1(Short bpztLimitCzasuK1) {
        this.bpztLimitCzasuK1 = bpztLimitCzasuK1;
    }

    @Basic
    @Column(name = "bpzt_tryb_pracy_k1")
    public Short getBpztTrybPracyK1() {
        return bpztTrybPracyK1;
    }

    public void setBpztTrybPracyK1(Short bpztTrybPracyK1) {
        this.bpztTrybPracyK1 = bpztTrybPracyK1;
    }

    @Basic
    @Column(name = "bpzt_czas_pracy_przerwa45_k1")
    public Integer getBpztCzasPracyPrzerwa45K1() {
        return bpztCzasPracyPrzerwa45K1;
    }

    public void setBpztCzasPracyPrzerwa45K1(Integer bpztCzasPracyPrzerwa45K1) {
        this.bpztCzasPracyPrzerwa45K1 = bpztCzasPracyPrzerwa45K1;
    }

    @Basic
    @Column(name = "bpzt_calkowity_czas_przerw_k1")
    public Integer getBpztCalkowityCzasPrzerwK1() {
        return bpztCalkowityCzasPrzerwK1;
    }

    public void setBpztCalkowityCzasPrzerwK1(Integer bpztCalkowityCzasPrzerwK1) {
        this.bpztCalkowityCzasPrzerwK1 = bpztCalkowityCzasPrzerwK1;
    }

    @Basic
    @Column(name = "bpzt_czas_aktywnosci_k1")
    public Integer getBpztCzasAktywnosciK1() {
        return bpztCzasAktywnosciK1;
    }

    public void setBpztCzasAktywnosciK1(Integer bpztCzasAktywnosciK1) {
        this.bpztCzasAktywnosciK1 = bpztCzasAktywnosciK1;
    }

    @Basic
    @Column(name = "bpzt_czas_jazd_2tygodnie_k1")
    public Integer getBpztCzasJazd2TygodnieK1() {
        return bpztCzasJazd2TygodnieK1;
    }

    public void setBpztCzasJazd2TygodnieK1(Integer bpztCzasJazd2TygodnieK1) {
        this.bpztCzasJazd2TygodnieK1 = bpztCzasJazd2TygodnieK1;
    }

    @Basic
    @Column(name = "bpzt_numer_karty_k2")
    public String getBpztNumerKartyK2() {
        return bpztNumerKartyK2;
    }

    public void setBpztNumerKartyK2(String bpztNumerKartyK2) {
        this.bpztNumerKartyK2 = bpztNumerKartyK2;
    }

    @Basic
    @Column(name = "bpzt_limit_czasu_k2")
    public Short getBpztLimitCzasuK2() {
        return bpztLimitCzasuK2;
    }

    public void setBpztLimitCzasuK2(Short bpztLimitCzasuK2) {
        this.bpztLimitCzasuK2 = bpztLimitCzasuK2;
    }

    @Basic
    @Column(name = "bpzt_tryb_pracy_k2")
    public Short getBpztTrybPracyK2() {
        return bpztTrybPracyK2;
    }

    public void setBpztTrybPracyK2(Short bpztTrybPracyK2) {
        this.bpztTrybPracyK2 = bpztTrybPracyK2;
    }

    @Basic
    @Column(name = "bpzt_czas_pracy_przerwa45_k2")
    public Integer getBpztCzasPracyPrzerwa45K2() {
        return bpztCzasPracyPrzerwa45K2;
    }

    public void setBpztCzasPracyPrzerwa45K2(Integer bpztCzasPracyPrzerwa45K2) {
        this.bpztCzasPracyPrzerwa45K2 = bpztCzasPracyPrzerwa45K2;
    }

    @Basic
    @Column(name = "bpzt_calkowity_czas_przerw_k2")
    public Integer getBpztCalkowityCzasPrzerwK2() {
        return bpztCalkowityCzasPrzerwK2;
    }

    public void setBpztCalkowityCzasPrzerwK2(Integer bpztCalkowityCzasPrzerwK2) {
        this.bpztCalkowityCzasPrzerwK2 = bpztCalkowityCzasPrzerwK2;
    }

    @Basic
    @Column(name = "bpzt_czas_aktywnosci_k2")
    public Integer getBpztCzasAktywnosciK2() {
        return bpztCzasAktywnosciK2;
    }

    public void setBpztCzasAktywnosciK2(Integer bpztCzasAktywnosciK2) {
        this.bpztCzasAktywnosciK2 = bpztCzasAktywnosciK2;
    }

    @Basic
    @Column(name = "bpzt_czas_jazd_2tygodnie_k2")
    public Integer getBpztCzasJazd2TygodnieK2() {
        return bpztCzasJazd2TygodnieK2;
    }

    public void setBpztCzasJazd2TygodnieK2(Integer bpztCzasJazd2TygodnieK2) {
        this.bpztCzasJazd2TygodnieK2 = bpztCzasJazd2TygodnieK2;
    }

    @Basic
    @Column(name = "bpzt_dane_dodatkowe")
    public Short getBpztDaneDodatkowe() {
        return bpztDaneDodatkowe;
    }

    public void setBpztDaneDodatkowe(Short bpztDaneDodatkowe) {
        this.bpztDaneDodatkowe = bpztDaneDodatkowe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BiezacaPozycjaTacho that = (BiezacaPozycjaTacho) o;

        if (bpztId != null ? !bpztId.equals(that.bpztId) : that.bpztId != null) return false;
        if (bpztPredkosc != null ? !bpztPredkosc.equals(that.bpztPredkosc) : that.bpztPredkosc != null) return false;
        if (bpztPrzebieg != null ? !bpztPrzebieg.equals(that.bpztPrzebieg) : that.bpztPrzebieg != null) return false;
        if (bpztSilnik != null ? !bpztSilnik.equals(that.bpztSilnik) : that.bpztSilnik != null) return false;
        if (bpztKierunekJazdy != null ? !bpztKierunekJazdy.equals(that.bpztKierunekJazdy) : that.bpztKierunekJazdy != null)
            return false;
        if (bpztObecneKartyKierowcow != null ? !bpztObecneKartyKierowcow.equals(that.bpztObecneKartyKierowcow) : that.bpztObecneKartyKierowcow != null)
            return false;
        if (bpztNumerKartyK1 != null ? !bpztNumerKartyK1.equals(that.bpztNumerKartyK1) : that.bpztNumerKartyK1 != null)
            return false;
        if (bpztLimitCzasuK1 != null ? !bpztLimitCzasuK1.equals(that.bpztLimitCzasuK1) : that.bpztLimitCzasuK1 != null)
            return false;
        if (bpztTrybPracyK1 != null ? !bpztTrybPracyK1.equals(that.bpztTrybPracyK1) : that.bpztTrybPracyK1 != null)
            return false;
        if (bpztCzasPracyPrzerwa45K1 != null ? !bpztCzasPracyPrzerwa45K1.equals(that.bpztCzasPracyPrzerwa45K1) : that.bpztCzasPracyPrzerwa45K1 != null)
            return false;
        if (bpztCalkowityCzasPrzerwK1 != null ? !bpztCalkowityCzasPrzerwK1.equals(that.bpztCalkowityCzasPrzerwK1) : that.bpztCalkowityCzasPrzerwK1 != null)
            return false;
        if (bpztCzasAktywnosciK1 != null ? !bpztCzasAktywnosciK1.equals(that.bpztCzasAktywnosciK1) : that.bpztCzasAktywnosciK1 != null)
            return false;
        if (bpztCzasJazd2TygodnieK1 != null ? !bpztCzasJazd2TygodnieK1.equals(that.bpztCzasJazd2TygodnieK1) : that.bpztCzasJazd2TygodnieK1 != null)
            return false;
        if (bpztNumerKartyK2 != null ? !bpztNumerKartyK2.equals(that.bpztNumerKartyK2) : that.bpztNumerKartyK2 != null)
            return false;
        if (bpztLimitCzasuK2 != null ? !bpztLimitCzasuK2.equals(that.bpztLimitCzasuK2) : that.bpztLimitCzasuK2 != null)
            return false;
        if (bpztTrybPracyK2 != null ? !bpztTrybPracyK2.equals(that.bpztTrybPracyK2) : that.bpztTrybPracyK2 != null)
            return false;
        if (bpztCzasPracyPrzerwa45K2 != null ? !bpztCzasPracyPrzerwa45K2.equals(that.bpztCzasPracyPrzerwa45K2) : that.bpztCzasPracyPrzerwa45K2 != null)
            return false;
        if (bpztCalkowityCzasPrzerwK2 != null ? !bpztCalkowityCzasPrzerwK2.equals(that.bpztCalkowityCzasPrzerwK2) : that.bpztCalkowityCzasPrzerwK2 != null)
            return false;
        if (bpztCzasAktywnosciK2 != null ? !bpztCzasAktywnosciK2.equals(that.bpztCzasAktywnosciK2) : that.bpztCzasAktywnosciK2 != null)
            return false;
        if (bpztCzasJazd2TygodnieK2 != null ? !bpztCzasJazd2TygodnieK2.equals(that.bpztCzasJazd2TygodnieK2) : that.bpztCzasJazd2TygodnieK2 != null)
            return false;
        if (bpztDaneDodatkowe != null ? !bpztDaneDodatkowe.equals(that.bpztDaneDodatkowe) : that.bpztDaneDodatkowe != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = bpztId != null ? bpztId.hashCode() : 0;
        result = 31 * result + (bpztPredkosc != null ? bpztPredkosc.hashCode() : 0);
        result = 31 * result + (bpztPrzebieg != null ? bpztPrzebieg.hashCode() : 0);
        result = 31 * result + (bpztSilnik != null ? bpztSilnik.hashCode() : 0);
        result = 31 * result + (bpztKierunekJazdy != null ? bpztKierunekJazdy.hashCode() : 0);
        result = 31 * result + (bpztObecneKartyKierowcow != null ? bpztObecneKartyKierowcow.hashCode() : 0);
        result = 31 * result + (bpztNumerKartyK1 != null ? bpztNumerKartyK1.hashCode() : 0);
        result = 31 * result + (bpztLimitCzasuK1 != null ? bpztLimitCzasuK1.hashCode() : 0);
        result = 31 * result + (bpztTrybPracyK1 != null ? bpztTrybPracyK1.hashCode() : 0);
        result = 31 * result + (bpztCzasPracyPrzerwa45K1 != null ? bpztCzasPracyPrzerwa45K1.hashCode() : 0);
        result = 31 * result + (bpztCalkowityCzasPrzerwK1 != null ? bpztCalkowityCzasPrzerwK1.hashCode() : 0);
        result = 31 * result + (bpztCzasAktywnosciK1 != null ? bpztCzasAktywnosciK1.hashCode() : 0);
        result = 31 * result + (bpztCzasJazd2TygodnieK1 != null ? bpztCzasJazd2TygodnieK1.hashCode() : 0);
        result = 31 * result + (bpztNumerKartyK2 != null ? bpztNumerKartyK2.hashCode() : 0);
        result = 31 * result + (bpztLimitCzasuK2 != null ? bpztLimitCzasuK2.hashCode() : 0);
        result = 31 * result + (bpztTrybPracyK2 != null ? bpztTrybPracyK2.hashCode() : 0);
        result = 31 * result + (bpztCzasPracyPrzerwa45K2 != null ? bpztCzasPracyPrzerwa45K2.hashCode() : 0);
        result = 31 * result + (bpztCalkowityCzasPrzerwK2 != null ? bpztCalkowityCzasPrzerwK2.hashCode() : 0);
        result = 31 * result + (bpztCzasAktywnosciK2 != null ? bpztCzasAktywnosciK2.hashCode() : 0);
        result = 31 * result + (bpztCzasJazd2TygodnieK2 != null ? bpztCzasJazd2TygodnieK2.hashCode() : 0);
        result = 31 * result + (bpztDaneDodatkowe != null ? bpztDaneDodatkowe.hashCode() : 0);
        return result;
    }
}
