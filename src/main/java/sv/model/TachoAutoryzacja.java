package sv.model;

import javax.persistence.*;

/**
 * Created by norbertl on 16.02.17.
 */
@Entity
@Table( name = "tacho_autoryzacja")
@SequenceGenerator( name = "SEQ_TACHO_AUTORYZACJA", sequenceName = "tacho_autoryzacja_tca_id_seq")
public class TachoAutoryzacja {

    private Integer id;
    private Integer idOdczytuTacho;
    private String pakietApdu;
    private Short numerPakietuApdu;
    private String odpowiedzKartyApdu;

    @Id
    @Column(name = "tca_id")
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SEQ_TACHO_AUTORYZACJA")
    public Integer getId() {
        return id;
    }

    @Column(name = "tca_odt_id")
    public Integer getIdOdczytuTacho() {
        return idOdczytuTacho;
    }

    @Column(name = "tca_pakiet_apdu")
    public String getPakietApdu() {
        return pakietApdu;
    }

    @Column(name = "tca_numer_pakietu_apdu")
    public Short getNumerPakietuApdu() {
        return numerPakietuApdu;
    }

    @Column(name = "tca_odpowiedz_karty_przeds")
    public String getOdpowiedzKartyApdu() {
        return odpowiedzKartyApdu;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIdOdczytuTacho(Integer idOdczytuTacho) {
        this.idOdczytuTacho = idOdczytuTacho;
    }

    public void setPakietApdu(String pakietApdu) {
        this.pakietApdu = pakietApdu;
    }

    public void setNumerPakietuApdu(Short numerPakietuApdu) {
        this.numerPakietuApdu = numerPakietuApdu;
    }

    public void setOdpowiedzKartyApdu(String odpowiedzKartyApdu) {
        this.odpowiedzKartyApdu = odpowiedzKartyApdu;
    }
}
