package sv.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by tymek on 27.04.17.
 */
@Entity
@Table(name = "rola")
public class Rola {
    private Integer rlaId;
    private String rlaNazwa;
    private Boolean rlaSuperRola;
    private Boolean rlaAudytSt;
    private Timestamp rlaAudytDt;
    private Timestamp rlaAudytDm;
    private Long rlaAudytUt;
    private Long rlaAudytUm;
    private Boolean rlaSzablon;
    private Boolean rlaAktywny;
    private String rlaOpis;
    private Long rlaObrId;
    private List<Uprawnienie> uprawnienia;

    @Id
    @Column(name = "rla_id")
    public Integer getRlaId() {
        return rlaId;
    }

    public void setRlaId(Integer rlaId) {
        this.rlaId = rlaId;
    }

    @Basic
    @Column(name = "rla_nazwa")
    public String getRlaNazwa() {
        return rlaNazwa;
    }

    public void setRlaNazwa(String rlaNazwa) {
        this.rlaNazwa = rlaNazwa;
    }

    @Basic
    @Column(name = "rla_super_rola")
    public Boolean getRlaSuperRola() {
        return rlaSuperRola;
    }

    public void setRlaSuperRola(Boolean rlaSuperRola) {
        this.rlaSuperRola = rlaSuperRola;
    }

    @Basic
    @Column(name = "rla_audyt_st")
    public Boolean getRlaAudytSt() {
        return rlaAudytSt;
    }

    public void setRlaAudytSt(Boolean rlaAudytSt) {
        this.rlaAudytSt = rlaAudytSt;
    }

    @Basic
    @Column(name = "rla_audyt_dt")
    public Timestamp getRlaAudytDt() {
        return rlaAudytDt;
    }

    public void setRlaAudytDt(Timestamp rlaAudytDt) {
        this.rlaAudytDt = rlaAudytDt;
    }

    @Basic
    @Column(name = "rla_audyt_dm")
    public Timestamp getRlaAudytDm() {
        return rlaAudytDm;
    }

    public void setRlaAudytDm(Timestamp rlaAudytDm) {
        this.rlaAudytDm = rlaAudytDm;
    }

    @Basic
    @Column(name = "rla_audyt_ut")
    public Long getRlaAudytUt() {
        return rlaAudytUt;
    }

    public void setRlaAudytUt(Long rlaAudytUt) {
        this.rlaAudytUt = rlaAudytUt;
    }

    @Basic
    @Column(name = "rla_audyt_um")
    public Long getRlaAudytUm() {
        return rlaAudytUm;
    }

    public void setRlaAudytUm(Long rlaAudytUm) {
        this.rlaAudytUm = rlaAudytUm;
    }

    @Basic
    @Column(name = "rla_szablon")
    public Boolean getRlaSzablon() {
        return rlaSzablon;
    }

    public void setRlaSzablon(Boolean rlaSzablon) {
        this.rlaSzablon = rlaSzablon;
    }

    @Basic
    @Column(name = "rla_aktywny")
    public Boolean getRlaAktywny() {
        return rlaAktywny;
    }

    public void setRlaAktywny(Boolean rlaAktywny) {
        this.rlaAktywny = rlaAktywny;
    }

    @Basic
    @Column(name = "rla_opis")
    public String getRlaOpis() {
        return rlaOpis;
    }

    public void setRlaOpis(String rlaOpis) {
        this.rlaOpis = rlaOpis;
    }

    @Basic
    @Column(name = "rla_obr_id")
    public Long getRlaObrId() {
        return rlaObrId;
    }

    public void setRlaObrId(Long rlaObrId) {
        this.rlaObrId = rlaObrId;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "rola_uprawnie", joinColumns = {
            @JoinColumn(name = "rup_rla_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "rup_upr_id",
                    nullable = false, updatable = false) })
    public List<Uprawnienie> getUprawnienia() {
        return uprawnienia;
    }

    public void setUprawnienia(List<Uprawnienie> uprawnienia) {
        this.uprawnienia = uprawnienia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rola rola = (Rola) o;

        if (rlaId != null ? !rlaId.equals(rola.rlaId) : rola.rlaId != null) return false;
        if (rlaNazwa != null ? !rlaNazwa.equals(rola.rlaNazwa) : rola.rlaNazwa != null) return false;
        if (rlaSuperRola != null ? !rlaSuperRola.equals(rola.rlaSuperRola) : rola.rlaSuperRola != null) return false;
        if (rlaAudytSt != null ? !rlaAudytSt.equals(rola.rlaAudytSt) : rola.rlaAudytSt != null) return false;
        if (rlaAudytDt != null ? !rlaAudytDt.equals(rola.rlaAudytDt) : rola.rlaAudytDt != null) return false;
        if (rlaAudytDm != null ? !rlaAudytDm.equals(rola.rlaAudytDm) : rola.rlaAudytDm != null) return false;
        if (rlaAudytUt != null ? !rlaAudytUt.equals(rola.rlaAudytUt) : rola.rlaAudytUt != null) return false;
        if (rlaAudytUm != null ? !rlaAudytUm.equals(rola.rlaAudytUm) : rola.rlaAudytUm != null) return false;
        if (rlaSzablon != null ? !rlaSzablon.equals(rola.rlaSzablon) : rola.rlaSzablon != null) return false;
        if (rlaAktywny != null ? !rlaAktywny.equals(rola.rlaAktywny) : rola.rlaAktywny != null) return false;
        if (rlaOpis != null ? !rlaOpis.equals(rola.rlaOpis) : rola.rlaOpis != null) return false;
        if (rlaObrId != null ? !rlaObrId.equals(rola.rlaObrId) : rola.rlaObrId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rlaId != null ? rlaId.hashCode() : 0;
        result = 31 * result + (rlaNazwa != null ? rlaNazwa.hashCode() : 0);
        result = 31 * result + (rlaSuperRola != null ? rlaSuperRola.hashCode() : 0);
        result = 31 * result + (rlaAudytSt != null ? rlaAudytSt.hashCode() : 0);
        result = 31 * result + (rlaAudytDt != null ? rlaAudytDt.hashCode() : 0);
        result = 31 * result + (rlaAudytDm != null ? rlaAudytDm.hashCode() : 0);
        result = 31 * result + (rlaAudytUt != null ? rlaAudytUt.hashCode() : 0);
        result = 31 * result + (rlaAudytUm != null ? rlaAudytUm.hashCode() : 0);
        result = 31 * result + (rlaSzablon != null ? rlaSzablon.hashCode() : 0);
        result = 31 * result + (rlaAktywny != null ? rlaAktywny.hashCode() : 0);
        result = 31 * result + (rlaOpis != null ? rlaOpis.hashCode() : 0);
        result = 31 * result + (rlaObrId != null ? rlaObrId.hashCode() : 0);
        return result;
    }
}
