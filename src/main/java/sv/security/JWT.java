package sv.security;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import sv.tacho.backend.Privileges;
import sv.tacho.backend.db.OdczytTachografuDAO;
import sv.tacho.services.model.LoginBackendModel;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import java.io.UnsupportedEncodingException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.EnumSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by tymek on 29.03.17.
 */

@Stateless
@Local
public class JWT {

    private static final Logger logger = Logger.getLogger( JWT.class.getSimpleName());

    static {
        logger.setLevel(Level.INFO);
    }

    @EJB
    private OdczytTachografuDAO odczytTachografuDAO;

    /**
     * Zwraca Json Web Token dla poprawnego loginu i hasła
     * @param username
     * @param password
     * @return JWT zawierający id klienta, id użytkownika i uprawnienia użytkownika
     * w przypadku poprawnego loginu i hasła. W przypadku niepoprawnych danych zwraca string "unauthorized",
     * a w przypadku błędu serwera zwraca pusty string.
     */
    public String getToken(String username, String password, String language) {
        try {
            LoginBackendModel loginBackendModel = odczytTachografuDAO.login(username, password);
            if (loginBackendModel == null) {
                return "unauthorized"; // Response.status(401).build();
            }
            PrivateKey key = RSAKeys.getInstance().getPrivate("../resources/private_key.der");
            String token = com.auth0.jwt.JWT.create()
                    .withIssuer("Supervisor")
                    .withClaim("uztId", loginBackendModel.getUztId())
                    .withClaim("klnId", loginBackendModel.getKlnId())
                    .withClaim("prvl", loginBackendModel.getPrivileges() != null? loginBackendModel.getPrivileges().toString() : "")
                    .withClaim("lang", language != null ? language : "pl")
                    .sign(Algorithm.RSA512((RSAPrivateKey)key));
            return token;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Zwraca obiekt klasy LoginBackendModel, zawierający dane o użytkowniku w przypadku poprawnego tokenu
     * W przypadku niepoprawnego tokenu (lub braku uprawnień) zwraca null
     * @param token
     * @return
     */
    public LoginBackendModel authorize(String token, EnumSet<Privileges> requiredPrvl) {
        logger.info("JWT.authorize(): token: " + token);
        try {
            LoginBackendModel model = new LoginBackendModel();
            PublicKey key = RSAKeys.getInstance().getPublic("../resources/public_key.der");
            JWTVerifier verifier = com.auth0.jwt.JWT.require(Algorithm.RSA512((RSAPublicKey)key))
                    .withIssuer("Supervisor")
                    .build(); //Reusable verifier instance
            DecodedJWT decodedJWT = verifier.verify(token);
            Claim claim = decodedJWT.getClaim("uztId");
            if (claim != null) {
                model.setUztId(claim.asInt());
            }
            claim = decodedJWT.getClaim("klnId");
            if (claim != null) {
                model.setKlnId(claim.asInt());
            }
            claim = decodedJWT.getClaim("prvl");
            if (claim == null) {
                return null; // Brak uprawnień - brak autoryzacji
            }
            EnumSet<Privileges> privileges = parsePrivileges(claim.asString());
            model.setPrivileges(privileges);
            if (!privileges.containsAll(requiredPrvl) ) {
                logger.info("Brak wymaganych uprawnień. Wymagano: " + requiredPrvl + ", otrzymano: " + privileges);
                return null;
            }
            return model;
        } catch (JWTVerificationException e){
            //Invalid signature/claims
            logger.warning(e.getClass() + ": " + e.getMessage());
            return null;
        } catch (UnsupportedEncodingException e) {
            logger.warning(e.getClass() + ": " + e.getMessage());
            return null;
        } catch (Exception e) {
            logger.warning(e.getClass() + ": " + e.getMessage());
            return null;
        }
    }

    /**
     * Wyodrębnia uprawnienia ze Stringu zawartego w jwt
     * @param privileges
     * @return
     */
    private EnumSet<Privileges> parsePrivileges(String privileges) {
        EnumSet<Privileges> result = EnumSet.noneOf(Privileges.class);
        logger.info("privileges: " + privileges);
        privileges = privileges.substring(1, privileges.length() - 1);
        String[] prvls = privileges.split(", ");
        for (String prvl: prvls) {
            for (Privileges prv : Privileges.values()) {
                if (prvl.equals(prv.toString())) {
                    result.add(prv);
                }
            }
        }
        logger.info("after parse: " + result.toString());
        return result;
    }
}
