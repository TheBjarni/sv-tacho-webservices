package sv.tacho.services;

import org.bouncycastle.util.Arrays;
import sv.model.*;
import sv.security.JWT;
import sv.tacho.backend.Privileges;
import sv.tacho.backend.db.OdczytTachografuDAO;
import sv.tacho.services.model.LoginBackendModel;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by tymek on 23.03.17.
 */

@Stateless
@Local( TachoReadServiceAPI.class)
public class TachoReadService implements TachoReadServiceAPI {

    private final static String dddRootReplacement = ResourceBundle.getBundle("settings").getString("dddRootReplacement");
    private final static String dddRootTarget = ResourceBundle.getBundle("settings").getString("dddRootTarget");
    // for reads from 2018 or older
    private final static String dddRootOldTarget = "/home/software/gprs";

    private static final Logger logger = Logger.getLogger( TachoReadService.class.getSimpleName());

    static {
        logger.setLevel(Level.INFO);
    }

    @EJB
    private OdczytTachografuDAO tachoDAO;

    @EJB
    private JWT jwt;

    @Override
    public Response getTachographsByClientId(String token, Long from, Long to) {
        EnumSet<Privileges> privileges = EnumSet.of(Privileges.TACHOGRAPHS, Privileges.TACHO_READS);
        LoginBackendModel model = authorize(token, privileges);
        if (model == null) {
            return Response.status(401).build();
        }

        List<Tachograf> tachographs = this.tachoDAO.getTachoByClientId(model.getKlnId(), from, to);
        if (tachographs == null) {
            return Response.status(204).build();

        }
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Tachograf tacho: tachographs) {
            JsonArrayBuilder readsBuilder = Json.createArrayBuilder();
            if (tacho.getOdczyty() != null) {
                logger.info("reads size: " + tacho.getOdczyty().size());
                for (OdczytTachografu odczyt : tacho.getOdczyty()) {
                    readsBuilder.add(readAsJson(odczyt));
                }
            } else {
                logger.warning("tacho.odczyty == null!");
            }
            Long mileage = getMileage(tacho);
            String lastPosition = getLastPosition(tacho);
            Long posTime = getLastPositionTime(tacho);

            if (tachoHasData(tacho)) {
                builder.add(Json.createObjectBuilder()
                        .add("id", tacho.getTchId())
                        .add("regNr", tacho.getTchOdtNrRejestracyjny())
                        .add("vin", tacho.getTchOdtVin())
                        .add("schedule", tachoDAO.getSchedule(tacho))
                        .add("lastRead", tacho.getDataOstatniegoOdczytu() != null ? tacho.getDataOstatniegoOdczytu().getTime() : 0)
                        .add("lastReadTry", tacho.getDataOstatniejProbyOdczytu() != null ? tacho.getDataOstatniejProbyOdczytu().getTime() : 0)
                        .add("lastAuth", tacho.getDataOstatniejAutoryzacji() != null ? tacho.getDataOstatniejAutoryzacji().getTime() : 0)
                        .add("readNow", tacho.getTchZlecenieOdczytuTeraz() != null ? tacho.getTchZlecenieOdczytuTeraz().getTime() : 0)
                        .add("mileage", mileage == null ? 0 : mileage)
                        .add("lastPos", lastPosition == null ? "" : lastPosition)
                        .add("posTime", posTime == null ? 0 : posTime)
                        .add("problem", tachoDAO.getProblemType(tacho.getOdczyty()))
                        .add("reads", readsBuilder)
                );
            }
        }
        return Response.ok(builder.build(), MediaType.APPLICATION_JSON_TYPE).build();
    }

    private boolean tachoHasData(Tachograf tacho) {
        return tacho.getTchOdtNrRejestracyjny() != null && !tacho.getTchOdtNrRejestracyjny().isEmpty()
                && tacho.getTchOdtVin() != null && !tacho.getTchOdtVin().isEmpty();
    }

    private long getLastRead(List<OdczytTachografu> odczyty) {
        long result = 0;
        if (odczyty == null) {
            return result;
        }
        for (OdczytTachografu odczyt: odczyty) {
            Date dataOdczytu = odczyt.getWyslanieZadaniaOdczytu();
            if (odczyt.getSukces() && dataOdczytu != null && dataOdczytu.getTime() > result) {
                result = dataOdczytu.getTime();
            }
        }
        return result;
    }

    private long getLastReadTry(List<OdczytTachografu> odczyty) {
        long result = 0;
        if (odczyty == null) {
            return result;
        }
        for (OdczytTachografu odczyt: odczyty) {
            if (odczyt.getWyslanieZadaniaOdczytu().getTime() > result) {
                result = odczyt.getWyslanieZadaniaOdczytu().getTime();
            }
        }
        return result;
    }

    private long getLastAuth(List<OdczytTachografu> odczyty) {
        long result = 0;
        if (odczyty == null) {
            return result;
        }
        for (OdczytTachografu odczyt: odczyty) {
            if (odczyt.getSukces() && odczyt.getAutoryzacjaKartyP() != null && odczyt.getAutoryzacjaKartyP().getTime() > result) {
                result = odczyt.getAutoryzacjaKartyP().getTime();
            }
        }
        return result;

    }

    @Override
    public Response getDriversByClientId(String token, Long from, Long to) {
        EnumSet<Privileges> privileges = EnumSet.of(Privileges.TACHOGRAPHS, Privileges.DRIVERS);
        LoginBackendModel model = authorize(token, privileges);
        if (model == null) {
            return Response.status(401).build();
        }
        List<TachoKierowcy> drivers = this.tachoDAO.getTachoDrivers(model.getKlnId(), from, to);
        if (drivers == null) {
            return Response.status(204).build();
        }
        logger.info("drivers size: " + drivers.size());
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (TachoKierowcy driver : drivers) {
            JsonArrayBuilder readsBuilder = Json.createArrayBuilder();
            if (driver.getOdczyty() != null) {
                logger.info("reads size: " + driver.getOdczyty().size());
                for (OdczytTachografu odczyt : driver.getOdczyty()) {
                    readsBuilder.add(readAsJson(odczyt));
                }
            } else {
                logger.warning("driver.odczyty == null!");
            }
            String name, surname;
            if (driver.getOdczyty().size() > 0) {
                OdczytTachografu odczyt = driver.getOdczyty().get(0);
                if (odczyt.getRodzaj() == 2) {
                    name = odczyt.getImieK1DDD();
                    surname = odczyt.getNazwiskoK1DDD();
                } else if (odczyt.getRodzaj() == 4) {
                    name = odczyt.getImieK2DDD();
                    surname = odczyt.getNazwiskoK2DDD();
                } else {
                    logger.warning("Niepoprawny odczyt dla kierowcy: " + driver.getTckNumerKarty());
                    name = surname = "";
                }
            } else {
                name = driver.getTckImieDDD() != null ? driver.getTckImieDDD() : "";
                surname = driver.getTckNazwiskoDDD() != null ? driver.getTckNazwiskoDDD() : "";
            }
            builder.add(Json.createObjectBuilder()
                    .add("id", driver.getTckId())
                    .add("name", name)
                    .add("surname", surname)
                    .add("cardNumber", driver.getTckNumerKarty() != null ? driver.getTckNumerKarty() : "")
                    .add("lastRead", getLastRead(driver.getOdczyty()))
                    .add("lastCarRegNr", driver.getOstatniPojazd() != null ? driver.getOstatniPojazd().getNrRejestracyjny() : "")
                    .add("lastPos", getLastPositionString(driver))
                    .add("posTime", driver.getTckOstatniaObecnosc() != null ? driver.getTckOstatniaObecnosc().getTime() : 0)
                    .add("posRegNr", driver.getNrRejOstatniejPozycji() != null ? driver.getNrRejOstatniejPozycji() : "")
                    .add("problem", tachoDAO.getProblemType(driver.getOdczyty()))
                    .add("adnId", driver.getTckOstatniPojazd() != null ? driver.getTckOstatniPojazd() : 0)
                    .add("readNow", driver.getTckOdczytajTeraz() != null ? driver.getTckOdczytajTeraz().getTime() : 0)
                    .add("reads", readsBuilder)
            );
        }
        return Response.ok(builder.build(), MediaType.APPLICATION_JSON_TYPE).build();
    }

    /**
     * Zwraca plik DDD dla tachografu o podanym id i typie, jeśli autoryzacja przebiegła pomyślnie.
     * @param token JWT przesyłany w nagłówku authorization
     * @param id Id odczytu tachografu / karty kierowcy
     * @param type 1 - odczyt tachografu, 2 - odczyt karty kierowcy 1, 4 - odczyt karty kierowcy 2
     * @return Odpowiedź http z możliwymi statusami:
     *         200 - poprawnie odczytany plik DDD
     *         401 - brak autoryzacji
     *         400 - niepoprawne parametry lub brak przynajmniej jednego z nich
     *         204 - brak pliku ddd dla podanych parametrów
     *         5** - błąd serwera
     */
    @Override
    public Response getDDD(String token, Integer id, Short type) {
        EnumSet<Privileges> privileges = EnumSet.of(Privileges.TACHOGRAPHS, Privileges.TACHO_READS, Privileges.TACHO_DOWNLOAD);
        LoginBackendModel model = authorize(token, privileges);
        // Nieudana autoryzacja
        if (model == null) {
            return Response.status(401).build();
        }
        // Brakujący lub niepoprawny parametr albo brak id klienta w tokenie
        if (id == null || type == null || model.getKlnId() == null || !Arrays.contains(new short[] {1, 2, 4}, type)) {
            return Response.status(400).build();
        }
        String path = tachoDAO.getDDDPath(id, type, model.getKlnId());
        if (path == null) {
            return Response.status(204).build();
        }
        // W przypadku braku właściwości dddRootReplacement w pliku settings.properties pobieram plik z oryginalnej ścieżki
        if (path.contains(dddRootTarget)) {
            path = path.replace(dddRootTarget, dddRootReplacement);
        } else {
            path = path.replace(dddRootOldTarget, dddRootReplacement);
        }
        logger.info("DDD's path: " + path);
        File file = new File(path);
        logger.info("File length: " + file.length());
        if (file.exists() && file.canRead() ) {
            return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition","attachment;filename=" + file.getName()).build();
        }
        return Response.status(204).build();
    }

    @Override
    public Response getPosition(String token, String cardNumber, Long adnId, Long time) {
        EnumSet<Privileges> privileges = EnumSet.of(Privileges.TACHOGRAPHS, Privileges.TACHO_READS, Privileges.TACHO_DOWNLOAD);
        LoginBackendModel model = authorize(token, privileges);
        // Nieudana autoryzacja
        if (model == null) {
            return Response.status(401).build();
        }
        boolean invalidQuery = !validCardNumber(cardNumber) || model.getKlnId() == null;
        if (invalidQuery) {
            return Response.status(400).build();
        }
        if (adnId == null || adnId == 0 || time == null || time == 0) {
            return Response.status(204).build();
        }
        BiezacaPozycja lastPositionWithDriver = tachoDAO.getLastPositionWithDriver(cardNumber, adnId, time);
        if (lastPositionWithDriver == null) {
            return Response.status(204).build();
        }
        JsonObjectBuilder builder = getPosition(lastPositionWithDriver);
        return Response.ok(builder.build(), MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    public Response readNow(String token, String cardNumber) {
        EnumSet<Privileges> privileges = EnumSet.of(Privileges.TACHOGRAPHS, Privileges.TACHO_READS, Privileges.TACHO_DOWNLOAD);
        LoginBackendModel model = authorize(token, privileges);
        // Nieudana autoryzacja
        if (model == null) {
            return Response.status(401).build();
        }
        boolean invalidQuery = !validCardNumber(cardNumber) || model.getKlnId() == null;
        if (invalidQuery) {
            logger.warning("cardNumber: " + cardNumber);
            return Response.status(400).build();
        }
        tachoDAO.readNow(cardNumber);
        return Response.ok().build();
    }
    
    private JsonObjectBuilder getPosition(BiezacaPozycja position) {
        return Json.createObjectBuilder().add("time", position.getBpzCzas().getTime())
                .add("country", position.getBpzKraj() == null ? "" : position.getBpzKraj())
                .add("province", position.getBpzWojewodztwo() == null ? "" : position.getBpzWojewodztwo())
                .add("city", position.getBpzMiejscowosc() == null ? "" : position.getBpzMiejscowosc())
                .add("street", position.getBpzUlica() == null ? "" : position.getBpzUlica())
                .add("regNr", position.getAuto() == null ? "" : position.getAuto().getNrRejestracyjny());
    }

    private boolean validCardNumber(String cardNumber) {
        return cardNumber != null && cardNumber.length() == 18 && cardNumber.matches("[a-zA-Z0-9_]*");
    }

    /**
     * Zwraca obiekt LoginBackendModel, gdy autoryzacja przebiegła pomyślnie.
     * W przeciwnym wypadku zwraca null.
     * @param token
     * @param privileges uprawnienia, jakie musi mieć użytkownik, żeby móc pobrać dane
     * @return
     */
    private LoginBackendModel authorize(String token, EnumSet<Privileges> privileges) {
        if (token == null) {
            return null;
        }
        return jwt.authorize(token.substring(token.indexOf(' ') + 1), privileges);
    }

    /**
     * Zwraca obiekt JSON z klasy OdczytTachografu
     * @param odczyt
     * @return
     */
    private JsonObjectBuilder readAsJson(OdczytTachografu odczyt) {
        JsonObjectBuilder builder = Json.createObjectBuilder().add("id", odczyt.getId());
        builder.add("readDate", odczyt.getWyslanieZadaniaOdczytu() != null ? odczyt.getWyslanieZadaniaOdczytu().getTime() : 0);
        builder.add("downloadDate", odczyt.getOstatniePobranieDanych() != null ? odczyt.getOstatniePobranieDanych().getTime() : 0)
                .add("authDate", odczyt.getAutoryzacjaKartyP() != null ? odczyt.getAutoryzacjaKartyP().getTime() : 0)
                .add("success", odczyt.getSukces() != null ? odczyt.getSukces() : false)
                .add("companyCard", odczyt.getNrKartyPrzedsiebiorstwaDDD() != null ? odczyt.getNrKartyPrzedsiebiorstwaDDD() : "")
                .add("dataStart", odczyt.getPoczatekDanychDDD() != null ? odczyt.getPoczatekDanychDDD().getTime() : 0)
                .add("dataEnd", odczyt.getKoniecDanychDDD() != null ? odczyt.getKoniecDanychDDD().getTime() : 0);
        return builder;
    }

    private Long getMileage(Tachograf tacho) {
        if (tacho == null) return null;
        if (tacho.getTchRejestratorSerial() == null) return null;
        if (tacho.getTchRejestratorSerial().getLastPosition() == null) return null;
        if (tacho.getTchRejestratorSerial().getLastPosition().getTachoPosition() == null) return null;
        return tacho.getTchRejestratorSerial().getLastPosition().getTachoPosition().getBpztPrzebieg();
    }

    private Long getLastPositionTime(Tachograf tacho) {
        if (tacho == null) return null;
        if (tacho.getTchRejestratorSerial() == null) return null;
        return this.getLastPositionTime(tacho.getTchRejestratorSerial());
    }

    private Long getLastPositionTime(Rejestrator rejestrator) {
        if (rejestrator.getLastPosition() == null) return null;
        if (rejestrator.getLastPosition().getBpzCzas() == null) return null;
        return rejestrator.getLastPosition().getBpzCzas().getTime();
    }

    private Long getLastPositionTime(String cardNumber) {
        BiezacaPozycja pozycja = tachoDAO.getLastPositionByCardNumber(cardNumber);
        if (pozycja == null) {
            return null;
        }
        return pozycja.getBpzCzas() == null ? null : pozycja.getBpzCzas().getTime();
    }

    private String getLastPosition(Tachograf tacho) {
        if (tacho == null) return null;
        if (tacho.getTchRejestratorSerial() == null) return null;
        return this.getLastPosition(tacho.getTchRejestratorSerial());
    }

    /**
     * Znajduje ostatnią pozycję dla pojazdu
     * @param rejestrator
     * @return
     */
    private String getLastPosition(Rejestrator rejestrator) {
        if (rejestrator.getLastPosition() == null) return null;
        BiezacaPozycja pozycja = rejestrator.getLastPosition();
        if (pozycja == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder(pozycja.getBpzKraj() == null ? "" : pozycja.getBpzKraj());
        builder.append(", ").append(pozycja.getBpzWojewodztwo()).append(", ").append(pozycja.getBpzMiejscowosc())
                .append(", ").append(pozycja.getBpzUlica());
        return builder.toString();
    }

    /**
     * Znajduje ostatnią pozycję dla kierowcy
     * @param cardNumber
     * @return
     */
    private String getLastPosition(String cardNumber) {
        BiezacaPozycja pozycja = tachoDAO.getLastPositionByCardNumber(cardNumber);
        if (pozycja == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder(pozycja.getBpzKraj());
        builder.append(", ").append(pozycja.getBpzWojewodztwo()).append(", ").append(pozycja.getBpzMiejscowosc())
                .append(", ").append(pozycja.getBpzUlica());
        return builder.toString();
    }

    private JsonObjectBuilder getLastPositionString(TachoKierowcy driver) {
        BiezacaPozycja pozycja = driver.getOstatniaPozycja();
        if (pozycja == null) {
            return Json.createObjectBuilder();
        }
        return getPosition(pozycja);
    }
}
