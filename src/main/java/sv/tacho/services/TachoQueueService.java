package sv.tacho.services;

import sv.model.OdczytTachografu;
import sv.model.TachoAutoryzacja;
import sv.security.JWT;
import sv.tacho.backend.Privileges;
import sv.tacho.backend.db.OdczytTachografuDAO;
import sv.tacho.backend.tools.ApduPacketsLogger;
import sv.tacho.services.model.CardResponseMessage;
import sv.tacho.services.model.LoginBackendModel;
import sv.tacho.services.model.TachoReadAuthStatus;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.EnumSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by norbertl on 12.02.17.
 */

@Stateless
@Local( TachoQueueServiceAPI.class)
public class TachoQueueService implements TachoQueueServiceAPI{

    private static final Logger logger = Logger.getLogger( TachoQueueService.class.getSimpleName());

    static {

        logger.setLevel(Level.INFO);
    }

    @EJB
    private ApduPacketsLogger recApduCache;

    @EJB
    private OdczytTachografuDAO tachoDAO;

    @EJB
    private JWT jwt;

    @Override
    public JsonObject getQueuedOperations(String token, Integer limit, boolean online, String status) {

        logger.fine( String.format("WS-kolejka zleceń, parametry żądania: limit = %s, online = %s, status = %s", limit, online, status));

        try {
            if (null == token) {
                return Json.createObjectBuilder().add( "items", Json.createArrayBuilder()).build(); // pusta odpowiedź
            }
            LoginBackendModel model = jwt.authorize(token.substring(token.indexOf(' ') + 1), EnumSet.noneOf(Privileges.class));

            if (null == model) {
                return Json.createObjectBuilder().add( "items", Json.createArrayBuilder()).build(); // pusta odpowiedź
            }

            OdczytTachografu newestQueuedTachoRead = tachoDAO.getNewestQueuedTachoRead(model.getKlnId());

            if ( newestQueuedTachoRead != null){

                JsonObjectBuilder generator = Json.createObjectBuilder();

                return generator.add( "items", Json.createArrayBuilder()
                        .add( Json.createObjectBuilder()
                                .add( "request", Json.createObjectBuilder()
                                        .add( "status", "queued")
                                        .add( "device", ""+newestQueuedTachoRead.getRejestratorSerial()))
                                .add("id", newestQueuedTachoRead.getId())))
                        .build();
            }

            return Json.createObjectBuilder().add( "items", Json.createArrayBuilder()).build(); // pusta odpowiedź
        } catch (Exception e) {
            e.printStackTrace();
            return Json.createObjectBuilder().build();
        }


    }

    @Override
    public JsonObject getTachoReadDetails(Integer id) {

        OdczytTachografu tachoReadById = this.tachoDAO.getTachoReadById(id);

        if ( tachoReadById != null){

            JsonObjectBuilder generator = Json.createObjectBuilder();

            return generator.add( "status", tachoReadById.getStanAutoryzacjiKartaP())
                            .add( "device", ""+tachoReadById.getRejestratorSerial())
                            .build();
        }

        return Json.createObjectBuilder().build(); // pusta odpowiedź
    }

    @Override
    public TachoReadAuthStatus updateTachoReadAuthStatus(Integer id, TachoReadAuthStatus authStatus) {

        this.tachoDAO.updateTachoRead( id, authStatus);

        return authStatus;
    }

    @Override
    public JsonObject getTachoApduCountAndRequestDetails(Integer id) {

        OdczytTachografu tachoReadById = this.tachoDAO.getTachoReadById(id);

        if ( tachoReadById != null){

            // pobranie licznika pakietów APDU

            int apduCount = this.tachoDAO.getApduCount(id, tachoReadById.getRejestratorSerial());

            JsonObjectBuilder generator = Json.createObjectBuilder();

            return generator.add( "request", Json.createObjectBuilder()
                                .add( "status", tachoReadById.getStanAutoryzacjiKartaP())
                                .add( "device", ""+tachoReadById.getRejestratorSerial()))
                            .add("apdu_count", apduCount)
                            .build();
        }

        return Json.createObjectBuilder().build(); // pusta odpowiedź
    }

    @Override
    public JsonObject getFirstAvailableApduPacket( Long recSerial) {

        TachoAutoryzacja packet = recApduCache.getApduPacket(recSerial);

        if ( packet != null){

            JsonObjectBuilder generator = Json.createObjectBuilder();

            return generator.add( "packet", Json.createObjectBuilder()
                        .add( "body", packet.getPakietApdu())
                        .add( "seqnum", packet.getNumerPakietuApdu()))
                    .build();
        }

        return Json.createObjectBuilder().build(); // pusta odpowiedź
    }

    @Override
    public JsonObject commitApduPacket( Long recSerial) {
        JsonObjectBuilder generator = Json.createObjectBuilder();

        return generator.add( "status", "OK").build();
    }

    @Override
    public JsonObject saveCardResponse(Long recSerial, CardResponseMessage cardResponse) {

        tachoDAO.updateApduPacket(recSerial, cardResponse);

        return commitApduPacket( recSerial);
    }

    @Override
    public JsonObject getClientStatus() {
        logger.info("getClientStatus()");
        JsonObjectBuilder generator = Json.createObjectBuilder();
        return generator.add( "status", "OK").build();
    }
}
