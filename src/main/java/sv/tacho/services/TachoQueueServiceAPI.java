package sv.tacho.services;

import sv.tacho.services.model.CardResponseMessage;
import sv.tacho.services.model.TachoReadAuthStatus;

import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by norbertl on 08.02.17.
 */

@Path("/api")
public interface TachoQueueServiceAPI {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("tachograph/v1/queue")
    public JsonObject getQueuedOperations(@HeaderParam("Authorization") String token,
                                          @QueryParam("limit") Integer limit, @QueryParam("online") boolean online, @QueryParam("status") String status);

    @GET
    @Path("tachograph/v1/requests/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getTachoReadDetails(@PathParam("requestId") Integer id);

    @POST
    @Path("tachograph/v1/requests/{requestId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public TachoReadAuthStatus updateTachoReadAuthStatus(@PathParam("requestId") Integer id, TachoReadAuthStatus authStatus);

    @GET
    @Path("tachograph/v1/apdu/{requestId}/status")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getTachoApduCountAndRequestDetails(@PathParam("requestId") Integer id);

    @POST
    @Path("tachograph/v1/apdu/{gpsId}/dequeue")
    @Consumes()
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getFirstAvailableApduPacket(@PathParam("gpsId") Long recSerial);

    @POST
    @Path("tachograph/v1/apdu/{gpsId}/commit")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject commitApduPacket(@PathParam("gpsId") Long recSerial);

    @POST
    @Path("tachograph/v1/apdu/{gpsId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public JsonObject saveCardResponse(@PathParam("gpsId") Long recSerial, CardResponseMessage cardResponse);

    @GET
    @Path("tachograph/v1/client-status")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getClientStatus();


}
