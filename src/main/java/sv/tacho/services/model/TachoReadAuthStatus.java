package sv.tacho.services.model;

/**
 * Created by norbertl on 18.02.17.
 */
public class TachoReadAuthStatus {

    private String status;
    private String device;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public TachoReadAuthStatus(String status, String device) {
        this.status = status;
        this.device = device;
    }

    public TachoReadAuthStatus() {
    }
}
