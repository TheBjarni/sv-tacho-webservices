package sv.tacho.services.model;

/**
 * Created by norbertl on 19.02.17.
 */
public class CardResponseMessage {

    private String packet;
    private int seqnum;

    public String getPacket() {
        return packet;
    }

    public void setPacket(String packet) {
        this.packet = packet;
    }

    public int getSeqnum() {
        return seqnum;
    }

    public void setSeqnum(int seqnum) {
        this.seqnum = seqnum;
    }
}
