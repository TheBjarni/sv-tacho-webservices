package sv.tacho.services.model;

import sv.tacho.backend.Privileges;

import java.util.EnumSet;

/**
 * Created by tymek on 14.03.17.
 */
public class LoginBackendModel {
    private Integer uztId;
    private Integer klnId;
    private String imie;
    private String nazwisko;
    private EnumSet<Privileges> privileges;

    public Integer getUztId() {
        return uztId;
    }

    public void setUztId(Integer uztId) {
        this.uztId = uztId;
    }

    public Integer getKlnId() {
        return klnId;
    }

    public void setKlnId(Integer klnId) {
        this.klnId = klnId;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public EnumSet<Privileges> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(EnumSet<Privileges> privileges) {
        this.privileges = privileges;
    }
}
