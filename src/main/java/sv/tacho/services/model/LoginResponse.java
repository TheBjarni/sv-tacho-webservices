package sv.tacho.services.model;

/**
 * Created by norbertl on 09.02.17.
 */
public class LoginResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginResponse() {
    }

    public LoginResponse(String message) {
        this.message = message;
    }
}
