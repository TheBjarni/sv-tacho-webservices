package sv.tacho.services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by tymek on 23.03.17.
 * Klasa zawierająca usługi potrzebne do interfejsu Odczyty tachografów.
 * Wszystkie usługi wymagają nagłówka Authorization z poprawnym tokenem.
 */
@Path("/tacho")
public interface TachoReadServiceAPI {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("tachographs")
    Response getTachographsByClientId(@HeaderParam("Authorization") String token,
                                      @QueryParam("from") Long from, @QueryParam("to") Long to);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("drivers")
    Response getDriversByClientId(@HeaderParam("Authorization") String token,
                                  @QueryParam("from") Long from, @QueryParam("to") Long to);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("ddd")
    Response getDDD(@HeaderParam("Authorization") String token,
                    @QueryParam("id") Integer id, @QueryParam("type") Short type);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("position")
    Response getPosition(@HeaderParam("Authorization") String token, @QueryParam("cardNumber") String cardNumber,
                    @QueryParam("adnId") Long adnId, @QueryParam("time") Long time);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("readNow")
    Response readNow(@HeaderParam("Authorization") String token, @FormParam("cardNumber") String cardNumber);

}
