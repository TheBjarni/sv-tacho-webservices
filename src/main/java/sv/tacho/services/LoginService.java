package sv.tacho.services;

import sv.security.JWT;
import sv.security.RSAKeys;
import sv.tacho.backend.db.OdczytTachografuDAO;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.PublicKey;
import java.util.logging.Logger;

/**
 *
 * Created by norbertl on 09.02.17.
 *
 */

@Stateless
@Local( LoginServiceAPI.class)
public class LoginService implements LoginServiceAPI{

    private static final Logger logger = Logger.getLogger( LoginService.class.getSimpleName());

    @EJB
    private OdczytTachografuDAO odczytTachografuDAO;

    @EJB
    private JWT jwt;

    @Override
    public Response login(String username, String password) {

        logger.info( String.format("WS-logowanie Próba zalogowania => %s / %s", username, password));

        String token = jwt.getToken(username, password, null);
        switch (token) {
            case "":
                return Response.serverError().build();
            case "unauthorized":
                return Response.status( Response.Status.UNAUTHORIZED).build();
            default:
                JsonObject result = Json.createObjectBuilder().add("message", "ok").build();
                return Response.ok(result, MediaType.APPLICATION_JSON_TYPE)
                        .header("Authorization", "Bearer " + token)
                        .build();
        }
    }

    @Override
    public Response login2(String username, String password, String language) {
        logger.info( String.format("WS-logowanie Próba zalogowania => %s / %s / %s", username, password, language));
        String token = jwt.getToken(username, password, language);
        switch (token) {
            case "":
                return Response.serverError().build();
            case "unauthorized":
                return Response.status(401).build();
            default:
                JsonObject result = Json.createObjectBuilder().add("message", "ok").build();
                return Response.ok(result, MediaType.APPLICATION_JSON_TYPE)
                        .header("Authorization", "Bearer " + token)
                        .build();
        }

    }

    @Override
    public Response logout(Cookie authorization) {
        if (authorization != null) {
            logger.info(authorization.getValue());
        } else {
            logger.info("authorization is null");
        }
        String token;
        PublicKey key = null;
        try {
            key = RSAKeys.getInstance().getPublic("../resources/public_key.der");
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }

        return Response.ok().build();
    }

}
