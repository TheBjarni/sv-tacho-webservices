package sv.tacho.services;

import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by norbertl on 08.02.17.
 */

@Path("/account")
public interface LoginServiceAPI {

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("zaloguj")
    public Response login( @FormParam("username") String username, @FormParam("password") String password);

    // Ta metoda będzie wywoływana w trakcie logowania do aplikacji głównej Supervisor
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("login")
    public Response login2( @QueryParam("username") String username, @QueryParam("password") String password, @QueryParam("lang") String language);

    @GET
    @Produces("text/plain")
    @Path("wyloguj")
    public Response logout( @CookieParam("authorization") Cookie authorization);

}
