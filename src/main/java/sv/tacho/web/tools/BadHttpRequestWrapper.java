package sv.tacho.web.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Created by norbertl on 20.02.17.
 */
public class BadHttpRequestWrapper extends HttpServletRequestWrapper{

    public BadHttpRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getHeader(String name) {

        if ("content-type".equals(name)){
            return "application/json; charset=UTF-8";
        }

        return super.getHeader(name);
    }

    @Override
    public Enumeration<String> getHeaders(String name) {

        if ("content-type".equals(name)){

            return Collections.enumeration(Arrays.asList("application/json; charset=UTF-8"));
        }

        return super.getHeaders(name);
    }
}
