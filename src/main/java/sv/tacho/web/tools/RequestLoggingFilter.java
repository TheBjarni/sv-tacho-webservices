package sv.tacho.web.tools;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by norbertl on 12.02.17.
 */
@WebFilter("/*")
public class RequestLoggingFilter implements Filter{

    private static final Logger logger = Logger.getLogger( RequestLoggingFilter.class.getSimpleName());

    static {

        logger.setLevel(Level.FINE);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

        logger.fine( String.format( "WS-request => %s, HTTP method = %s",httpRequest.getRequestURI(), httpRequest.getMethod()));

        Enumeration<String> headerNames = httpRequest.getHeaderNames();

        while ( headerNames.hasMoreElements()){

            String headerName = headerNames.nextElement();
            logger.fine(String.format(" -> %s - %s", headerName, httpRequest.getHeader( headerName)));
        }

        if ( httpRequest.getRequestURI().contains("dequeue")){
            servletRequest = new BadHttpRequestWrapper( httpRequest);
            logger.fine("Zamieniam http requests w celu wyłączenia błędu");
        }
        filterChain.doFilter( servletRequest, servletResponse);

        logger.fine( String.format("Response => %s",httpResponse.getStatus()));
    }

    @Override
    public void destroy() {

    }
}
