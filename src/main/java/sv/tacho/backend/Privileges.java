package sv.tacho.backend;

/**
 * Created by tymek on 29.03.17.
 */
public enum Privileges {
    TACHO_READS,
    TACHOGRAPHS,
    DRIVERS,
    TACHO_DOWNLOAD,
    DRIVERS_DOWNLOAD
}
