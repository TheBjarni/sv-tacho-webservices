package sv.tacho.backend.db;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by norbertl on 17.02.17.
 */
//@Singleton
//@Startup
public class DbTests {

    private final Logger logger = Logger.getLogger(DbTests.class.getSimpleName());

    @PersistenceContext(unitName = "svProd")
    private EntityManager em;


    @PostConstruct
    public void testOnLoad(){

        Query query = em.createQuery("select ot from OdczytTachografu ot where ot.stanAutoryzacjiKartaP = 'queued'");
        List resultList = query.getResultList();

        logger.info( String.format(" Rozmiar listy odczytów w stanie queued = %s",resultList.size()));
    }
}
