package sv.tacho.backend.db;

import sv.model.*;
import sv.tacho.backend.Privileges;
import sv.tacho.backend.tools.ApduPacketsLogger;
import sv.tacho.services.model.CardResponseMessage;
import sv.tacho.services.model.LoginBackendModel;
import sv.tacho.services.model.TachoReadAuthStatus;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by norbertl on 17.02.17.
 */
@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OdczytTachografuDAO {
    private static final String PRV_TACHO = "tacho";
    private static final String PRV_TACHO_TACHOGRAPHS = "tacho:tachografy";
    private static final String PRV_TACHO_TACHOGRAPHS_DOWNL = "tacho:tachografy:pobierz";
    private static final String PRV_TACHO_DRIVERS = "tacho:kierowcy";
    private static final String PRV_TACHO_DRIVERS_DOWNL = "tacho:kierowcy:pobierz";
    private static final Short[] TACHO_READS = { 1, 3, 5, 7};

    private final Logger logger = Logger.getLogger(OdczytTachografuDAO.class.getSimpleName());

    @EJB
    private ApduPacketsLogger recApduCache;

    @PersistenceContext(unitName = "svProd")
    private EntityManager em;

    public OdczytTachografu getNewestQueuedTachoRead(Integer klnId) {

        OdczytTachografu result = null;

        Query query = em.createQuery("select ot from OdczytTachografu ot where ot.stanAutoryzacjiKartaP = 'queued' order by ot.wyslanieZadaniaOdczytu desc");
        query = em.createQuery("select t from Tachograf t where t.tchKlnId = :klnId")
                .setParameter("klnId", klnId);

        List tachos = query.getResultList();

        if ( !tachos.isEmpty()){
            List<Integer> tachoIds = new ArrayList();
            for (Tachograf tachograf : (List<Tachograf>)tachos) {
                tachoIds.add(tachograf.getTchId());
            }
            query = em.createQuery("select ot from OdczytTachografu ot where ot.idTachografu in :tachoIds" +
                    " and ot.stanAutoryzacjiKartaP = 'queued' order by ot.wyslanieZadaniaOdczytu desc")
                    .setParameter("tachoIds", tachoIds);

            List resultList = query.getResultList();
            if (!resultList.isEmpty()) {
                result = (OdczytTachografu) resultList.get(0);
            }
        }

        return result;
    }

    public OdczytTachografu getTachoReadById(Integer id){

        OdczytTachografu odczytTachografu = em.find(OdczytTachografu.class, id);
        this.em.refresh( odczytTachografu);

        return odczytTachografu;
    }

    public void updateTachoRead(Integer id, TachoReadAuthStatus readAuthStatus){

        OdczytTachografu odczytTachografu = em.find(OdczytTachografu.class, id);

        odczytTachografu.setStanAutoryzacjiKartaP( readAuthStatus.getStatus());
    }

    public int getApduCount(Integer tachoReadId, Long rejestratorSerial){

        List<TachoAutoryzacja> apduPacketToAuthorize = getApduPacketToAuthorize(tachoReadId);

        if ( apduPacketToAuthorize != null && !apduPacketToAuthorize.isEmpty()){

            recApduCache.setRecApduIdRecord( rejestratorSerial, apduPacketToAuthorize.get(0));

            return apduPacketToAuthorize.size();
        }

        return 0;
    }


    private List<TachoAutoryzacja> getApduPacketToAuthorize(Integer tachoReadId){

        Query query = em.createQuery("select ta from TachoAutoryzacja ta where ta.idOdczytuTacho = :tachoReadId and ta.odpowiedzKartyApdu is null order by ta.numerPakietuApdu asc");

        query.setParameter("tachoReadId", tachoReadId);

        return query.getResultList();
    }

    public void updateApduPacket(Long rejestratorSerial, CardResponseMessage cardResponse){

        TachoAutoryzacja apduPacket = recApduCache.getApduPacket(rejestratorSerial);

        if ( apduPacket != null){

            logger.info("AUTOR_ODPOWIEDZ_KARTY długość odpowiedzi z karty "+cardResponse.getPacket().length());
            logger.info("AUTOR_ODPOWIEDZ_KARTY => "+cardResponse.getPacket());
            apduPacket.setOdpowiedzKartyApdu( cardResponse.getPacket());
        }

        this.em.merge( apduPacket);
    }

    public OdczytTachografu[] getTachoReads(Long rejestratorSerial) {
        OdczytTachografu[] result = null;
        return result;
    }

    public LoginBackendModel login(String username, String password) {
        LoginBackendModel result = new LoginBackendModel();
        Query query = em.createQuery("select u from Uzytkownik u where u.login = :login").setParameter("login", username);
        Uzytkownik uzytkownik;
        try {
            uzytkownik = (Uzytkownik) query.getSingleResult();
        } catch (NoResultException e) {     // Nieprawidłowy login
            logger.warning("Nieudana próba logowania, login: " + username);
            return null;
        }
        if (uzytkownik == null || !uzytkownik.getHaslo().equals(password)) {    // Nieprawidłowe hasło
            logger.warning("Nieudana próba logowania, login: " + username + ", hasło: " + password);
            return null;
        }
        result.setUztId(uzytkownik.getId());
        if (uzytkownik.getPrcId() != null) {
            result.setKlnId(getClientIdByEmployeeId(uzytkownik.getPrcId()));
        }
        query = em.createQuery("select r from Rola r where r.rlaId = :id")
                .setParameter("id", uzytkownik.getRlaId());
        Rola rola = (Rola) query.getSingleResult();
        List<Uprawnienie> uprawnienia = rola.getUprawnienia();
        EnumSet<Privileges> privileges = EnumSet.noneOf(Privileges.class);
        for (Uprawnienie uprawnienie : uprawnienia) {
            if (uprawnienie.getUprNazwa().equals(PRV_TACHO)) {
                privileges.add(Privileges.TACHOGRAPHS);
            }
            if (uprawnienie.getUprNazwa().equals(PRV_TACHO_TACHOGRAPHS)) {
                privileges.add(Privileges.TACHO_READS);
            }
            if (uprawnienie.getUprNazwa().equals(PRV_TACHO_TACHOGRAPHS_DOWNL)) {
                privileges.add(Privileges.TACHO_DOWNLOAD);
            }
            if (uprawnienie.getUprNazwa().equals(PRV_TACHO_DRIVERS)) {
                privileges.add(Privileges.DRIVERS);
            }
            if (uprawnienie.getUprNazwa().equals(PRV_TACHO_DRIVERS_DOWNL)) {
                privileges.add(Privileges.DRIVERS_DOWNLOAD);
            }
        }
        logger.info("Uprawnienia: " + privileges);
        result.setPrivileges(privileges);
        return result;
    }

    private Integer getClientIdByEmployeeId(Integer userId) throws NoResultException {
        Query query = em.createQuery("select p from Pracownik p where p.id = :id").setParameter("id", userId);
        Pracownik pracownik;
        try {
            pracownik = (Pracownik) query.getSingleResult();
        } catch (NoResultException e) {     // Brak pracownika odpowiadającego danemu id
            return null;
        }
        if (pracownik == null) return null;
        return pracownik.getKlnId();
    }

    public List<Tachograf> getTachoByClientId(Integer klnId, Long from, Long to) {
        logger.info("klnId: " + klnId);
        if (from == null || to == null) {
            return null;
        }
        Query query = em.createQuery(
            "select od from OdczytTachografu od where (od.dataOdczytuTachoDDD between :from and :to " +
                    "or od.wyslanieZadaniaOdczytu between :from and :to) and od.klnId = :klnId and od.rodzaj in (1, 3, 5, 7)")
            .setParameter("klnId", klnId)
            .setParameter("from", new Date(from)).setParameter("to", new Date(to));
        List<OdczytTachografu> odczyty = query.getResultList();
        logger.info("liczba odczytów: " + odczyty.size());
        if (odczyty.size() == 0) {
            return new ArrayList<>();
        }
        List<Integer> tachoIds = new ArrayList<>();
        for (OdczytTachografu odczyt : odczyty) {
            odczyt.setRodzaj((short)1);
            if (!tachoIds.contains(odczyt.getIdTachografu())) {
                tachoIds.add(odczyt.getIdTachografu());
            }
        }
        query = em.createQuery(
                "select distinct t from Tachograf t left join t.tchRejestratorSerial rej, rej.lastPosition pos, t.odczyty od" +
                " where t.tchId in :ids")
                .setParameter("ids", tachoIds);
        List<Tachograf> result = query.getResultList();
        List<Tachograf> toAdd = new ArrayList<>();
        for (Tachograf tacho: result) {
            fillDates(tacho);
            tacho.setOdczyty(new ArrayList<>());
        }
        for (OdczytTachografu odczyt : odczyty) {
            Tachograf tacho = find(result, odczyt.getNrRejestracyjnyDDD(), odczyt.getVinDDD());
            if (tacho == null) {
                tacho = find(toAdd, odczyt.getNrRejestracyjnyDDD(), odczyt.getVinDDD());
            }
            if (tacho == null) {
                tacho = find(result, odczyt.getIdTachografu());
                if (tacho == null) {    // Nie powinno wystąpić
                    continue;
                }
                if (tacho.getTchOdtNrRejestracyjny() != null && tacho.getTchOdtVin() != null) {
                    tacho = tacho.clone();
                    toAdd.add(tacho);
                }
                tacho.setTchOdtNrRejestracyjny(odczyt.getNrRejestracyjnyDDD());
                tacho.setTchOdtVin(odczyt.getVinDDD());
            }
            tacho.getOdczyty().add(odczyt);
        }
        result.addAll(toAdd);
        for (Iterator<Tachograf> iter = result.iterator(); iter.hasNext();) {
            Tachograf tacho = iter.next();
            if (tacho.getOdczyty().size() == 0 || tacho.getTchOdtNrRejestracyjny() == null) {
                iter.remove();
            }
        }
        return result;
    }

    /**
     * Uzupełnia informacje o ostatniej dacie odczytu, ostatniej próbie odczytu i ostatniej autoryzacji tachografu.
     * @param tacho
     */
    private void fillDates(Tachograf tacho) {
        List<OdczytTachografu> odczyty = tacho.getOdczyty();
        for (OdczytTachografu odczyt: odczyty) {
            if (!isTachoRead(odczyt)) {
                continue;
            }
            if (tacho.getDataOstatniegoOdczytu() == null && odczyt.getSukces()) {
                tacho.setDataOstatniegoOdczytu(odczyt.getWyslanieZadaniaOdczytu());
            } else if (odczyt.getWyslanieZadaniaOdczytu() != null  && odczyt.getSukces()
                    && odczyt.getWyslanieZadaniaOdczytu().getTime() > tacho.getDataOstatniegoOdczytu().getTime()) {
                tacho.setDataOstatniegoOdczytu(odczyt.getWyslanieZadaniaOdczytu());
            }

            if (tacho.getDataOstatniejProbyOdczytu() == null) {
                tacho.setDataOstatniejProbyOdczytu(odczyt.getWyslanieZadaniaOdczytu());
            } else if (odczyt.getWyslanieZadaniaOdczytu() != null && odczyt.getWyslanieZadaniaOdczytu().getTime() > tacho.getDataOstatniejProbyOdczytu().getTime()) {
                tacho.setDataOstatniejProbyOdczytu(odczyt.getWyslanieZadaniaOdczytu());
            }

            if (tacho.getDataOstatniejAutoryzacji() == null) {
                tacho.setDataOstatniejAutoryzacji(odczyt.getAutoryzacjaKartyP());
            } else if (odczyt.getAutoryzacjaKartyP() != null && odczyt.getAutoryzacjaKartyP().getTime() > tacho.getDataOstatniejAutoryzacji().getTime()) {
                tacho.setDataOstatniejAutoryzacji(odczyt.getAutoryzacjaKartyP());
            }
        }
    }

    // Zwraca true wtedy i tylko wtedy, gdy odczyt zawiera odczyt tachografu
    private boolean isTachoRead(OdczytTachografu odczyt) {
        List types = Arrays.asList(1, 3, 5, 7);
        return types.contains((int)odczyt.getRodzaj());
    }

    /**
     * Znajduje tachograf po id tachografu
     * @param tachos
     * @param idTachografu
     * @return
     */
    private Tachograf find(List<Tachograf> tachos, Integer idTachografu) {
        for (Tachograf tacho: tachos) {
            if (tacho.getTchId().equals(idTachografu)) {
                return tacho;
            }
        }
        return null;
    }

    /**
     * Znajduje tachograf po numerze rejestracyjnym i VIN
     * @param tachos
     * @param nrRejestracyjnyDDD
     * @param vinDDD
     * @return
     */
    private Tachograf find(List<Tachograf> tachos, String nrRejestracyjnyDDD, String vinDDD) {
        for (Tachograf tacho: tachos) {
            if (tacho.getTchOdtNrRejestracyjny() != null && tacho.getTchOdtVin() != null
                    && tacho.getTchOdtNrRejestracyjny().equals(nrRejestracyjnyDDD) && tacho.getTchOdtVin().equals(vinDDD)) {
                return tacho;
            }
        }
        return null;
    }

    public String getSchedule(Tachograf tacho) {
        if (tacho == null) {
            return "";
        }
        if (tacho.getTchOdczytyCoIleDni() != null) {
            return tacho.getTchOdczytyCoIleDni().toString() + "d";
        }
        if (tacho.getTchOdczytyKtoryTydzien() != null) {
            return tacho.getTchOdczytyKtoryTydzien().toString() + "t";
        }
        if (tacho.getTchOdczytyIleRazyWMiesiacu() != null) {
            return tacho.getTchOdczytyIleRazyWMiesiacu() + "m";
        }
        return "";
    }

    public String getProblemType(List<OdczytTachografu> odczyty) {
        if (odczyty.size() == 0) {
            return "unknown";
        }
        OdczytTachografu last = odczyty.get(0);
        for (OdczytTachografu odczyt: odczyty) {
            if (odczyt.getWyslanieZadaniaOdczytu().getTime() > last.getWyslanieZadaniaOdczytu().getTime()) {
                last = odczyt;
            }
        }
        if (last.getSukces()) {
            return "ok";
        }
        if (last.getTimeout()) {
            return "timeout";
        }
        if (last.getPrzerwano()) {
            return "break";
        }
        return "unknown";
    }

    public String getDDDPath(Integer id, Short type, Integer klnId) {
        Query query = em.createQuery("select o from OdczytTachografu o where o.id = :id")
                .setParameter("id", id);
        OdczytTachografu result = (OdczytTachografu) query.getSingleResult();
        if (result == null || !result.getKlnId().equals(klnId)) {
            return null;
        }
        em.createQuery("update OdczytTachografu set ostatniePobranieDanych = :now where id = :id")
                .setParameter("id", id).setParameter("now", new Date()).executeUpdate();
        switch (type) {
            case 1:
                return result.getSciezkaDoPliku();
            case 2:
                return result.getSciezkaDoPlikuK1();
            case 4:
                return result.getSciezkaDoPlikuK2();
            default:
                return null;
        }
    }

    public List<TachoKierowcy> getTachoDrivers(Integer klnId, Long from, Long to) {
        logger.info("klnId: " + klnId);
        if (from == null || to == null) {
            return null;
        }
        Query query = em.createQuery(
                "select od from OdczytTachografu od where (od.dataOdczytuK1DDD between :from and :to " +
                    "or od.wyslanieZadaniaOdczytu between :from and :to) and od.klnId = :klnId and od.sciezkaDoPlikuK1 is not null")
                .setParameter("klnId", klnId)
                .setParameter("from", new Date(from)).setParameter("to", new Date(to));
        List<OdczytTachografu> odczytyK1 = query.getResultList();
        query = em.createQuery(
                "select od from OdczytTachografu od where (od.dataOdczytuK2DDD between :from and :to " +
                    "or od.wyslanieZadaniaOdczytu between :from and :to) and od.klnId = :klnId and od.sciezkaDoPlikuK2 is not null")
                .setParameter("klnId", klnId)
                .setParameter("from", new Date(from)).setParameter("to", new Date(to));
        List<OdczytTachografu> odczytyK2 = query.getResultList();
        List<String> cardNumbers = new ArrayList<>();
        List<OdczytTachografu> odczytyK1Sklonowane = new ArrayList<>(odczytyK1.size());
        for (OdczytTachografu odczyt : odczytyK1) {
            try {
                OdczytTachografu read = (OdczytTachografu) odczyt.clone();
                read.setRodzaj((short) 2);
                odczytyK1Sklonowane.add(read);
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            if (odczyt.getNumerKartyK1DDD() != null && !cardNumbers.contains(odczyt.getNumerKartyK1DDD())) {
                cardNumbers.add(odczyt.getNumerKartyK1DDD());
            }
        }
        for (OdczytTachografu odczyt : odczytyK2) {
            odczyt.setRodzaj((short)4);
            if (odczyt.getNumerKartyK2DDD() != null && !cardNumbers.contains(odczyt.getNumerKartyK2DDD())) {
                cardNumbers.add(odczyt.getNumerKartyK2DDD());
            }
        }
        // Korekcja - w tabeli odczyty numery są bez prefiksów, a w tabeli tacho_kierowcy z prefiksami
        List<String> correctedNumbers = new ArrayList<>();
        for (String cardNumber : cardNumbers) {
            if (!cardNumber.substring(0, 2).equals("PL")) {
                correctedNumbers.add("PL" + cardNumber);
            }
        }
        logger.info("card numbers: " + correctedNumbers);
        if (correctedNumbers.size() == 0) {
            return new ArrayList<>();
        }
        query = em.createQuery("select t from TachoKierowcy t where t.tckNumerKarty in :cardNumbers")
                .setParameter("cardNumbers", correctedNumbers);
        List<TachoKierowcy> drivers = query.getResultList();
        for (TachoKierowcy driver : drivers) {
            driver.setOdczyty(new ArrayList<>());
            for (OdczytTachografu odczyt : odczytyK1Sklonowane) {
                if (odczyt.getNumerKartyK1DDD() != null && driver.getTckNumerKarty().contains(odczyt.getNumerKartyK1DDD())) {
                    driver.getOdczyty().add(odczyt);
                }
            }
            for (OdczytTachografu odczyt : odczytyK2) {
                if (odczyt.getNumerKartyK2DDD() != null && driver.getTckNumerKarty().contains(odczyt.getNumerKartyK2DDD())) {
                    driver.getOdczyty().add(odczyt);
                }
            }
        }
        return drivers;
    }

    /**
     * Uzupełnia informacje o ostatniej dacie odczytu karty kierowcy
     * @param driver
     */
    private void fillDates(TachoKierowcy driver) {
        List<OdczytTachografu> odczyty = driver.getOdczyty();
        for (OdczytTachografu odczyt: odczyty) {
            if (driver.getDataOstatniegoOdczytu() == null && odczyt.getSukces()) {
                driver.setDataOstatniegoOdczytu(odczyt.getDataOdczytuTachoDDD());
            } else if (odczyt.getDataOdczytuTachoDDD() != null  && odczyt.getSukces()
                    && odczyt.getDataOdczytuTachoDDD().getTime() > driver.getDataOstatniegoOdczytu().getTime()) {
                driver.setDataOstatniegoOdczytu(odczyt.getDataOdczytuTachoDDD());
            }
        }
    }

    public BiezacaPozycja getLastPositionByCardNumber(String cardNumber) {
        if (cardNumber == null) {
            return null;
        }
        Query query = em.createQuery(
                "select b from BiezacaPozycja b left join BiezacaPozycjaTacho bpt on b.bpzId = bpt.bpztId where "
                + "bpt.bpztNumerKartyK1 = :cardNumber or bpt.bpztNumerKartyK2 = :cardNumber order by b.bpzCzas desc ")
                .setParameter("cardNumber", cardNumber);
        List<BiezacaPozycja> result = query.getResultList();
        if (result != null && result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public Rejestrator getRejestrator(Long rejestratorSerial) {
        Query query = em.createQuery("select r from Rejestrator r where r.rejestratorSerial = :serial")
                .setParameter("serial", rejestratorSerial);
        return (Rejestrator)query.getSingleResult();
    }

    public BiezacaPozycja getLastPositionWithDriver(String cardNumber, Long adnId, Long time) {
        return (BiezacaPozycja) em.createQuery("select b from BiezacaPozycja b inner join BiezacaPozycjaTacho bpt on b.bpzId = bpt.bpztId where"
                + " (bpt.bpztNumerKartyK1 = :cardNumber or bpt.bpztNumerKartyK2 = :cardNumber) and b.bpzAdnId = :adnId and b.bpzCzas <= :time order by b.bpzCzas desc ")
                .setParameter("cardNumber", cardNumber)
                .setParameter("adnId", adnId)
                .setParameter("time", new Date(time))
                .setMaxResults(1)
                .getSingleResult();
    }

    public void readNow(String cardNumber) {
        em.createQuery("update TachoKierowcy set tckOdczytajTeraz = :now where tckNumerKarty = :cardNumber")
                .setParameter("now", new Date())
                .setParameter("cardNumber", cardNumber)
                .executeUpdate();
    }
}
