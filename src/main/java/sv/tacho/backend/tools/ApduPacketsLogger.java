package sv.tacho.backend.tools;

import sv.model.TachoAutoryzacja;

import javax.ejb.Singleton;
import java.util.HashMap;
import java.util.Map;

/**
 * Klasa pamiętająca o ostatnich pakietach APDU, które mają zostać wysłane do karty przedsiębiorstwa
 *
 */
@Singleton
public class ApduPacketsLogger {

    private Map<Long, TachoAutoryzacja> recApduIdMap = new HashMap<>();

    public void setRecApduIdRecord( Long recSerial, TachoAutoryzacja apduPacket){

        recApduIdMap.put( recSerial, apduPacket);
    }

    public TachoAutoryzacja getApduPacket( Long recSerial){
        return recApduIdMap.get( recSerial);
    }

}
