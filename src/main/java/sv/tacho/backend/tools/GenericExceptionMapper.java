package sv.tacho.backend.tools;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by norbertl on 20.02.17.
 */
public class GenericExceptionMapper implements ExceptionMapper<Throwable>{

    @Override
    public Response toResponse(Throwable throwable) {

        try{

            throwable.printStackTrace();

        } catch (Throwable e){

            System.out.println(e.getMessage());
        }

        return Response.ok().build();
    }
}
