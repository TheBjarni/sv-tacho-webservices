package sv.tacho;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by norbertl on 09.02.17.
 */
@ApplicationPath("api")
public class RestApplicationConfiguration extends Application {
}
