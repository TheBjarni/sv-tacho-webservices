import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import sv.model.Tachograf;
import sv.security.RSAKeys;
import sv.tacho.backend.db.OdczytTachografuDAO;
import sv.tacho.services.model.LoginBackendModel;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import java.io.UnsupportedEncodingException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tymek on 27.03.17.
 */
public class DBTest {
    private static OdczytTachografuDAO odczytTachografuDAO = new OdczytTachografuDAO();

    private static JsonArray test() {
        List<Tachograf> tachographs = new ArrayList<>(); // this.tachoDAO.getTachoByClientId(klnId);
        if (tachographs == null) {
            return Json.createArrayBuilder().build();
        }
        JsonArrayBuilder builder = Json.createArrayBuilder();
        tachographs.forEach(tacho -> builder.add(Json.createObjectBuilder()
                        .add("id", tacho.getTchId())
        ));
        return builder.build();
    }



    public static void main(String[] args) {
        LoginBackendModel model = jwtTest("eyJhbGciOiJSUzUxMiJ9.eyJ1enRJZCI6IjQxNzUxIiwiaXNzIjoiU3VwZXJ2aXNvciIsImtsbklkIjoiMCJ9.v3YYyyho0FvH75h84_txZ_2qdWew7NL1_bxviQFlVAqA7cFqcSE_xEzHYB-JxwZo-By2mzOwoX9876orQeB-_Ss41YOSO3tPFaP0TNSLIAOAoJufUgkrqIuX0NV46HDIe-JTjRhlY1WZcxwhMFLPAxLNvVjW73UR-dGNpUcHFtd2mg_nF5Y8GRK4RHrV6TmC44C79x8vJ02XanKSMzpP0e6zLGt1km7zDZ3o1CT_2Xn4QSWHFx0_UpAv7GgZAdczDYi1mSWpvgSwmuh-iGdGdJIMBJ8lsaP1B4AVvSc1nXTgdLInVK0lcF6VBNfEC6CjTwI7HgkCEf6xGDqMCEByHA");
        System.out.println(model);
    }

    private static LoginBackendModel jwtTest(String token) {
        try {
            LoginBackendModel model = new LoginBackendModel();
            System.out.println(System.getProperty("user.dir"));
            PublicKey key = RSAKeys.getInstance().getPublic("src/test/resources/public_key.der");
            JWTVerifier verifier = com.auth0.jwt.JWT.require(Algorithm.RSA512((RSAPublicKey)key))
                    .withIssuer("Supervisor")
                    .build(); //Reusable verifier instance
            DecodedJWT decodedJWT = verifier.verify(token);
            Claim claim = decodedJWT.getClaim("uztId");
            if (claim != null) {
                model.setUztId(Integer.parseInt(claim.asString()));
            }
            claim = decodedJWT.getClaim("klnId");
            if (claim != null) {
                model.setKlnId(Integer.parseInt(claim.asString()));
            }
            return model;
        } catch (JWTVerificationException exception){
            //Invalid signature/claims
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
